# Vultr infrastructure resources

resource "tls_private_key" "global_key" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "local_file" "private_key_pem" {
  filename = "${path.module}/id_rsa"
  sensitive_content = tls_private_key.global_key.private_key_pem
  file_permission = "0600"
}

resource "local_file" "public_key_openssh" {
  filename = "${path.module}/id_rsa.pub"
  content = tls_private_key.global_key.public_key_openssh
}

resource "vultr_ssh_key" "ssh_key" {
  name = "${var.prefix}-public-ssh-key"
  ssh_key = local_file.public_key_openssh.content
}

resource "vultr_server" "manager1" {
  region_id = var.vultr_region_new_york
  plan_id = var.vm_size_4gb_ram_2cpu
  os_id = var.operating_system_centos7
  label = "main-${var.manager}-${var.new_york}"
  hostname = "main-${var.manager}-${var.new_york}"
  ssh_key_ids = [
    vultr_ssh_key.ssh_key.id]

  connection {
    type = var.connection_type
    host = vultr_server.manager1.main_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "remote-exec" {
    inline = var.post_start_script_main_manager
  }

  provisioner "file" {
    source = "../../vault/config/vault-config.json"
    destination = "/var/vault/config/vault.json"
  }

  provisioner "file" {
    source = "../../traefik/traefik-portainer-compose.yml"
    destination = "/var/traefik/traefik-portainer-compose.yml"
  }

  provisioner "file" {
    source = "../../traefik/traefik.yml"
    destination = "/var/traefik/traefik.yml"
  }

  provisioner "file" {
    source = "../../prometheus/prometheus.yml"
    destination = "/var/prometheus/prometheus.yml"
  }

  provisioner "file" {
    source = "../../grafana/provisioning"
    destination = "/var/grafana_data/"
  }

  provisioner "file" {
    source = "../../sonarqube/sonarqube-community-branch-plugin-1.6.0.jar"
    destination = "/var/sonarqube/lib_common/sonarqube-community-branch-plugin-1.6.0.jar"
  }
}

resource "vultr_server" "manager2" {
  region_id = var.vultr_region_dallas
  plan_id = var.vm_size_1gb_ram_1cpu
  os_id = var.operating_system_centos7
  label = "${var.manager}-${var.dallas}"
  hostname = "${var.manager}-${var.dallas}"
  ssh_key_ids = [vultr_ssh_key.ssh_key.id]

  connection {
    type = var.connection_type
    host = vultr_server.manager2.main_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "file" {
    source = "../../docker_token.txt"
    destination = "/root/docker_token.txt"
  }

  provisioner "remote-exec" {
    inline = var.post_start_script_manager
  }
}

resource "vultr_server" "manager3" {
  region_id = var.vultr_region_seattle
  plan_id = var.vm_size_1gb_ram_1cpu
  os_id = var.operating_system_centos7
  label = "${var.manager}-${var.seattle}"
  hostname = "${var.manager}-${var.seattle}"
  ssh_key_ids = [vultr_ssh_key.ssh_key.id]

  connection {
    type = var.connection_type
    host = vultr_server.manager3.main_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "file" {
    source = "../../docker_token.txt"
    destination = "/root/docker_token.txt"
  }

  provisioner "remote-exec" {
    inline = var.post_start_script_manager
  }
}

resource "vultr_server" "worker1" {
  region_id = var.vultr_region_atlanta
  plan_id = var.vm_size_2gb_ram_1cpu
  os_id = var.operating_system_centos7
  label = "${var.worker}-${var.atlanta}"
  hostname = "${var.worker}-${var.atlanta}"
  ssh_key_ids = [vultr_ssh_key.ssh_key.id]

  connection {
    type = var.connection_type
    host = vultr_server.worker1.main_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "file" {
    source = "../../docker_token.txt"
    destination = "/root/docker_token.txt"
  }

  provisioner "remote-exec" {
    inline = var.post_start_script_worker
  }
}

resource "vultr_server" "worker2" {
  region_id = var.vultr_region_chicago
  plan_id = var.vm_size_2gb_ram_1cpu
  os_id = var.operating_system_centos7
  label = "${var.worker}-${var.chicago}"
  hostname = "${var.worker}-${var.chicago}"
  ssh_key_ids = [vultr_ssh_key.ssh_key.id]

  connection {
    type = var.connection_type
    host = vultr_server.worker2.main_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "file" {
    source = "../../docker_token.txt"
    destination = "/root/docker_token.txt"
  }

  provisioner "remote-exec" {
    inline = var.post_start_script_worker
  }
}