variable "vultr_token" {
  type = string
  description = "Vultr API token used to create infrastructure"
}

variable "vultr_region_new_york" {
  type = string
  description = "Vultr region used for workers"
  default = "1"
}

variable "vultr_region_atlanta" {
  type = string
  description = "Vultr region used for workers"
  default = "6"
}

variable "vultr_region_dallas" {
  type = string
  description = "Vultr region used for managers"
  default = "3"
}

variable "vultr_region_chicago" {
  type = string
  description = "Vultr region used for managers"
  default = "2"
}

variable "vultr_region_los_angeles" {
  type = string
  description = "Vultr region used for managers"
  default = "5"
}

variable "vultr_region_miami" {
  type = string
  description = "Vultr region used for managers"
  default = "39"
}

variable "vultr_region_seattle" {
  type = string
  description = "Vultr region used for managers"
  default = "4"
}

variable "vultr_region_silicon_valley" {
  type = string
  description = "Vultr region used for managers"
  default = "12"
}

variable "vultr_region_toronto" {
  type = string
  description = "Vultr region used for managers"
  default = "22"
}

variable "prefix" {
  type = string
  description = "Prefix added to names of all resources"
  default = "genericappdomain"
}

variable "vm_size_1gb_ram_1cpu" {
  type = string
  description = "Price per month $5"
  default = "201"
}

variable "vm_size_2gb_ram_1cpu" {
  type = string
  description = "Price per month $10"
  default = "202"
}

variable "vm_size_4gb_ram_2cpu" {
  type = string
  description = "Price per month $20"
  default = "203"
}

variable "vm_size_8gb_ram_4cpu" {
  type = string
  description = "Price per month $40"
  default = "204"
}

variable "operating_system_centos7" {
  type = string
  description = "Operating System"
  default = "167"
}

variable "user" {
  type = string
  description = "Default user"
  default = "root"
}

variable "connection_type" {
  type = string
  description = "SSH connection"
  default = "ssh"
}

variable "manager" {
  type = string
  description = "Manager"
  default = "manager"
}

variable "worker" {
  type = string
  description = "Worker"
  default = "worker"
}

variable "new_york" {
  type = string
  description = "Hostname"
  default = "New-York"
}

variable "dallas" {
  type = string
  description = "Hostname"
  default = "Dallas"
}

variable "seattle" {
  type = string
  description = "Hostname"
  default = "Seattle"
}

variable "atlanta" {
  type = string
  description = "Hostname"
  default = "Atlanta"
}

variable "chicago" {
  type = string
  description = "Hostname"
  default = "Chicago"
}

variable "post_start_script_main_manager" {
  type = list(string)
  description = "Setting up main manager"
  default = [
    "sudo mkdir /var/jenkins_home",
    "sudo mkdir /var/sonaqube_db_data",
    "sudo mkdir /var/sonarqube",
    "sudo mkdir -p /var/sonarqube/{sonarqube_extensions,sonarqube_data,sonarqube_logs,lib_common}",
    "sudo mkdir /var/portainer_data",
    "sudo mkdir /var/traefik",
    "sudo mkdir /var/traefik/letsencrypt",
    "sudo touch /var/traefik/letsencrypt/acme.json",
    "sudo mkdir /var/vault",
    "sudo mkdir -p /var/vault/{config,policies,file,logs}",
    "sudo mkdir /var/prometheus",
    "sudo mkdir /var/prometheus/prometheus_data",
    "sudo mkdir /var/grafana_data",
    "sudo yum update -y",
    "sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine",
    "sudo yum install -y yum-utils",
    "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
    "sudo yum install -y docker-ce docker-ce-cli containerd.io",
    "sudo systemctl start docker",
    "sudo systemctl enable docker",
    "sudo docker run hello-world",
    "sudo docker system prune -fa",
    "sudo timedatectl set-timezone America/New_York",
    "sudo yum install -y ntp",
    "sudo systemctl start ntpd",
    "sudo systemctl enable ntpd",
    "sudo systemctl enable ntpd",
    "sudo yum install -y firewalld",
    "sudo systemctl unmask firewalld",
    "sudo systemctl enable firewalld",
    "sudo systemctl start firewalld",
    "for i in 22 80 443 2376 2377 7946; do sudo firewall-cmd --add-port=$${i}/tcp --permanent; done",
    "for i in 4789 7946; do sudo firewall-cmd --add-port=$${i}/udp --permanent; done",
    "sudo firewall-cmd --reload",
    "sudo sysctl -w vm.max_map_count=262144",
    "sudo echo vm.max_map_count=262144 >> /etc/sysctl.conf",
    "sudo docker swarm init",
    "sudo docker network create --opt encrypted --driver overlay --attachable sandbox",
    "sudo chmod 600 /var/traefik/letsencrypt/acme.json",
    "sudo chown 1000 /var/jenkins_home",
    "sudo chmod 600 /var/sonaqube_db_data",
    "sudo chmod -R 600 /var/sonarqube",
    "sudo chmod 600 /var/portainer_data",
    "sudo chmod -R 600 /var/vault",
    "sudo chown -R 30000 /var/prometheus",
    "sudo chown -R 472 /var/grafana_data",
  ]
}

variable "post_start_script_manager" {
  type = list(string)
  description = "Setting up server"
  default = [
    "sudo yum update -y",
    "sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine",
    "sudo yum install -y yum-utils",
    "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
    "sudo yum install -y docker-ce docker-ce-cli containerd.io",
    "sudo systemctl start docker",
    "sudo systemctl enable docker",
    "sudo docker run hello-world",
    "sudo docker system prune -fa",
    "cat /root/docker_token.txt | docker login --username dockerhubusername --password-stdin",
    "sudo rm -rf /root/docker_token.txt",
    "sudo timedatectl set-timezone America/New_York",
    "sudo yum install -y ntp",
    "sudo systemctl start ntpd",
    "sudo systemctl enable ntpd",
    "sudo systemctl enable ntpd",
    "sudo yum install -y firewalld",
    "sudo systemctl unmask firewalld",
    "sudo systemctl enable firewalld",
    "sudo systemctl start firewalld",
    "for i in 22 80 443 2376 2377 7946; do sudo firewall-cmd --add-port=$${i}/tcp --permanent; done",
    "for i in 4789 7946; do sudo firewall-cmd --add-port=$${i}/udp --permanent; done",
    "sudo firewall-cmd --reload",
    "sudo sysctl -w vm.max_map_count=262144",
  ]
}

variable "post_start_script_worker" {
  type = list(string)
  description = "Setting up worker"
  default = [
    "sudo yum update -y",
    "sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine",
    "sudo yum install -y yum-utils",
    "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
    "sudo yum install -y docker-ce docker-ce-cli containerd.io",
    "sudo systemctl start docker",
    "sudo systemctl enable docker",
    "sudo docker run hello-world",
    "sudo docker system prune -fa",
    "cat /root/docker_token.txt | docker login --username dockerhubusername --password-stdin",
    "sudo rm -rf /root/docker_token.txt",
    "sudo timedatectl set-timezone America/New_York",
    "sudo yum install -y ntp",
    "sudo systemctl start ntpd",
    "sudo systemctl enable ntpd",
    "sudo systemctl enable ntpd",
    "sudo yum install -y firewalld",
    "sudo systemctl unmask firewalld",
    "sudo systemctl enable firewalld",
    "sudo systemctl start firewalld",
    "for i in 22 2376 2377 7946; do sudo firewall-cmd --add-port=$${i}/tcp --permanent; done",
    "for i in 4789 7946; do sudo firewall-cmd --add-port=$${i}/udp --permanent; done",
    "sudo firewall-cmd --reload",
    "sudo sysctl -w vm.max_map_count=262144",
    "sudo mkdir /var/jenkins_home",
  ]
}
