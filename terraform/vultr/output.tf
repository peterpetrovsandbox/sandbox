
output "vultr_manager1" {
  value = vultr_server.manager1
}

output "vultr_manager2" {
  value = vultr_server.manager2
}

output "vultr_manager3" {
  value = vultr_server.manager3
}

output "vultr_worker1" {
  value = vultr_server.worker1
}

output "vultr_worker2" {
  value = vultr_server.worker2
}