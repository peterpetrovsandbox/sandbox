provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.aws_us_east_1_region
}

provider "cloudflare" {
  version = "~> 2.0"
  email = var.email
  api_token = var.api_token
}

provider "tls" {
}