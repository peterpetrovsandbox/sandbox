variable "access_key" {
  type = string
  description = "AWS access key"
}

variable "secret_key" {
  type = string
  description = "AWS secret key"
}

variable "aws_us_east_1_region" {
  description = "AWS region to launch servers"
  default     = "us-east-1"
}

variable "aws_us_east_2_region" {
  description = "AWS region to launch servers"
  default     = "us-east-2"
}

variable "aws_us_west_1_region" {
  description = "AWS region to launch servers"
  default     = "us-west-1"
}

variable "aws_us_west_2_region" {
  description = "AWS region to launch servers"
  default     = "us-west-2"
}

variable "ubuntu_2004_amis" {
  type = map(string)
  default = {
    us-east-1 = "ami-0be3f0371736d5394"
    us-east-2 = "ami-0a8251430bac13e68"
    us-west-1 = "ami-05ddb1bcba9ace858"
    us-west-2 = "ami-0a8251430bac13e68"
  }
}

variable "availability_zone_us_east_1a" {
  type = string
  default = "us-east-1a"
}

variable "t2_nano" {
  type = string
  description = "1vCPU 0.5GB $0.0058 per hour"
  default = "t2.nano"
}

variable "t2_micro" {
  type = string
  description = "1vCPU 1GB $0.0116 per hour"
  default = "t2.micro"
}

variable "t2_small" {
  type = string
  description = "1vCPU 2GB $0.023 per hour"
  default = "t2.small"
}

variable "t2_medium" {
  type = string
  description = "2vCPU 4GB $0.0464 per hour"
  default = "t2.medium"
}

variable "t2_large" {
  type = string
  description = "2vCPU 8GB $0.0928 per hour"
  default = "t2.large"
}

variable "cidr_allow_all" {
  type = string
  description = "CIDR notation for all traffic"
  default = "0.0.0.0/0"
}

variable "cidr_allow_single_ip_only" {
  type = string
  description = "CIDR notation used for ingress from one IP only"
}

variable "connection_type" {
  type = string
  description = "SSH connection"
  default = "ssh"
}

variable "user" {
  type = string
  description = "Default user"
  default = "ubuntu"
}

variable "cluster_manager_count" {
  type = number
  description = "Swarm manager nodes count"
  default = 2
}

variable "cluster_worker_count" {
  type = number
  description = "Swarm worker nodes count"
  default = 2
}

variable "gp2_type" {
  type = string
  description = "General purpose AWS EBS volume type"
  default = "gp2"
}

variable "main_manager-post-start-provisioning" {
  type = list(string)
  description = "Setting up main manager"
  default = [
    "sudo docker swarm init",
    "sudo docker swarm join-token --quiet worker >> worker-token.txt",
    "sudo docker swarm join-token --quiet manager >> manager-token.txt",
    "sudo sed -i 's/PasswordAuthentication.*/PasswordAuthentication no/' /etc/ssh/sshd_config",
    "sudo mkdir ~/traefik",
    "sudo mkdir ~/traefik/letsencrypt",
    "sudo touch ~/traefik/letsencrypt/acme.json",
    "sudo mkdir ~/vault",
    "sudo mkdir ~/vault/config",
    "sudo mkdir ~/prometheus",
    "sudo mkdir ~/grafana",
    "sudo mkdir ~/sonarqube",
    "sudo mkdir ~/sonarqube/lib_common",
    "sudo chmod -R 777 ~/traefik",
    "sudo chmod -R 777 ~/vault",
    "sudo chmod -R 777 ~/prometheus",
    "sudo chmod -R 777 ~/grafana",
    "sudo chmod -R 777 ~/sonarqube",
  ]
}

variable "main_manager-provision-rexray-volumes" {
  type = list(string)
  description = "Provisioning rexray volumes"
  default = [
    "sudo chmod 600 ~/traefik/letsencrypt/acme.json",
    "sudo docker run -tid --volume-driver=rexray/ebs:0.11.4 -v traefik:/mystore --name temp01 busybox:1.33.0",
    "sudo docker cp traefik/. temp01:/mystore",

    "sudo docker run -tid --volume-driver=rexray/ebs:0.11.4 -v vault:/mystore --name temp02 busybox:1.33.0",
    "sudo docker cp vault/. temp02:/mystore",

    "sudo docker run -tid --volume-driver=rexray/ebs:0.11.4 -v prometheus:/mystore --name temp03 busybox:1.33.0",
    "sudo docker cp prometheus/. temp03:/mystore",

    "sudo docker run -tid --volume-driver=rexray/ebs:0.11.4 -v grafana:/mystore --name temp04 busybox:1.33.0",
    "sudo docker cp grafana/. temp04:/mystore",

    "sudo docker run -tid --volume-driver=rexray/ebs:0.11.4 -v sonarqube:/mystore --name temp05 busybox:1.33.0",
    "sudo docker cp sonarqube/. temp05:/mystore",

    "sudo docker stop $(sudo docker ps -a -q)",
    "sudo docker system prune -fa",

    "sudo rm -rf ~/traefik",
    "sudo rm -rf ~/vault",
    "sudo rm -rf ~/prometheus",
    "sudo rm -rf ~/grafana",
    "sudo rm -rf ~/sonarqube",

    "sudo docker network create --opt encrypted --driver overlay --attachable sandbox",
  ]
}