resource "tls_private_key" "global_key" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "local_file" "private_key_pem" {
  filename = "${path.module}/id_rsa"
  sensitive_content = tls_private_key.global_key.private_key_pem
  file_permission = "0600"
}

resource "local_file" "public_key_openssh" {
  filename = "${path.module}/id_rsa.pub"
  content = tls_private_key.global_key.public_key_openssh
}

resource "aws_key_pair" "ssh_key" {
  key_name = "public-ssh-key"
  public_key = local_file.public_key_openssh.content
}

resource "aws_vpc" "sandbox-vpc" {
  cidr_block           = "192.168.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "sandbox-vpc"
  }
}

resource "aws_subnet" "sandbox-public-subnet-1" {
  vpc_id                  = aws_vpc.sandbox-vpc.id
  cidr_block              = "192.168.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zone_us_east_1a

  tags = {
    Name = "sandbox-public-1"
  }
}

resource "aws_internet_gateway" "sandbox-internet-gateway" {
  vpc_id = aws_vpc.sandbox-vpc.id

  tags = {
    Name = "sandbox-internet-gateway"
  }
}

resource "aws_route_table" "sandbox-public-route-table" {
  vpc_id = aws_vpc.sandbox-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.sandbox-internet-gateway.id
  }

  tags = {
    Name = "sandbox-public-route-table"
  }
}

resource "aws_main_route_table_association" "sandbox-main-route-table-association" {
  vpc_id = aws_vpc.sandbox-vpc.id
  route_table_id = aws_route_table.sandbox-public-route-table.id
}

resource "aws_security_group" "sandbox-manager-security-group" {
  vpc_id = aws_vpc.sandbox-vpc.id
  name = "manager-security-group"
  description = "allow manager ingress egress"

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [var.cidr_allow_single_ip_only]
  }

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 443
    protocol = "tcp"
    to_port = 443
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 2049
    protocol = "tcp"
    to_port = 2049
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 2376
    protocol = "tcp"
    to_port = 2376
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 2377
    protocol = "tcp"
    to_port = 2377
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 7946
    protocol = "tcp"
    to_port = 7946
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 4789
    protocol = "udp"
    to_port = 4789
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 7946
    protocol = "udp"
    to_port = 7946
    cidr_blocks = [var.cidr_allow_all]
  }

  tags = {
    Name = "allow manager ingress egress"
  }
}

resource "aws_security_group" "sandbox-worker-security-group" {
  vpc_id = aws_vpc.sandbox-vpc.id
  name = "worker-security-group"
  description = "allow worker ingress egress"

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [var.cidr_allow_single_ip_only]
  }

  ingress {
    from_port = 2049
    protocol = "tcp"
    to_port = 2049
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 2376
    protocol = "tcp"
    to_port = 2376
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 2377
    protocol = "tcp"
    to_port = 2377
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 7946
    protocol = "tcp"
    to_port = 7946
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 4789
    protocol = "udp"
    to_port = 4789
    cidr_blocks = [var.cidr_allow_all]
  }

  ingress {
    from_port = 7946
    protocol = "udp"
    to_port = 7946
    cidr_blocks = [var.cidr_allow_all]
  }

  tags = {
    Name = "allow worker ingress egress"
  }
}

resource "aws_security_group" "efs-security-group" {
  name = "efs-security-group"
  description = "Allow traffic to NFS from instances on the VPC"
  vpc_id = aws_vpc.sandbox-vpc.id

  ingress {
    from_port = 2049
    protocol = "tcp"
    to_port = 2049

    cidr_blocks = [
      aws_vpc.sandbox-vpc.cidr_block
    ]
  }

  egress {
    from_port = 2049
    protocol = "tcp"
    to_port = 2049

    cidr_blocks = [
      aws_vpc.sandbox-vpc.cidr_block
    ]
  }

  tags = {
    Name = "Allow NFS EC2"
  }
}

resource "aws_ebs_volume" "traefik" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "traefik"
  }
}

resource "aws_ebs_volume" "portainer" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "portainer"
  }
}

resource "aws_ebs_volume" "jenkins" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "jenkins"
  }
}

resource "aws_ebs_volume" "grafana" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "grafana"
  }
}

resource "aws_ebs_volume" "prometheus" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "prometheus"
  }
}

resource "aws_ebs_volume" "sonarqube" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "sonarqube"
  }
}

resource "aws_ebs_volume" "sonarqube_db" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "sonarqube_db"
  }
}

resource "aws_ebs_volume" "vault" {
  availability_zone = var.availability_zone_us_east_1a
  size = 1
  type = var.gp2_type

  tags = {
    Name = "vault"
  }
}

resource "aws_efs_file_system" "sandbox_efs" {
  performance_mode = "generalPurpose"

  tags = {
    Name = "sandbox efs"
  }
}

resource "aws_efs_mount_target" "efs_mount_target" {
  file_system_id = aws_efs_file_system.sandbox_efs.id
  subnet_id = aws_instance.main_manager.subnet_id
  security_groups = [aws_security_group.sandbox-manager-security-group.id]
}

resource "aws_instance" "main_manager" {
  count = 1
  ami = "******"
  instance_type = var.t2_micro
  subnet_id = aws_subnet.sandbox-public-subnet-1.id
  vpc_security_group_ids = [aws_security_group.sandbox-manager-security-group.id]
  key_name = aws_key_pair.ssh_key.id

  connection {
    type = var.connection_type
    host = self.public_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "remote-exec" {
    inline = var.main_manager-post-start-provisioning
  }

  provisioner "file" {
    source = "../../traefik/traefik.yml"
    destination = "~/traefik/traefik.yml"
  }

  provisioner "file" {
    source = "../../traefik/traefik-portainer-aws-compose.yml"
    destination = "~/traefik-portainer-aws-compose.yml"
  }

  provisioner "file" {
    source = "../../vault/config/vault-config.json"
    destination = "~/vault/config/vault.json"
  }

  provisioner "file" {
    source = "../../prometheus/prometheus.yml"
    destination = "~/prometheus/prometheus.yml"
  }

  provisioner "file" {
    source = "../../grafana/provisioning"
    destination = "~/grafana/"
  }

  provisioner "file" {
    source = "../../sonarqube/sonarqube-community-branch-plugin-1.6.0.jar"
    destination = "~/sonarqube/lib_common/sonarqube-community-branch-plugin-1.6.0.jar"
  }

  provisioner "remote-exec" {
    inline = var.main_manager-provision-rexray-volumes
  }
}

resource "aws_instance" "manager" {
  count = var.cluster_manager_count
  ami = "******"
  instance_type = var.t2_nano
  subnet_id = aws_subnet.sandbox-public-subnet-1.id
  vpc_security_group_ids = [aws_security_group.sandbox-manager-security-group.id]
  key_name = aws_key_pair.ssh_key.id

  connection {
    type = var.connection_type
    host = self.public_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
//      "sudo docker swarm join ${aws_instance.main_manager.0.private_ip}:2377 --token $(sudo docker -H ${aws_instance.main_manager.0.private_ip} swarm join-token -q manager)",
      "sudo docker swarm join --token $(cat manager-token.txt) ${aws_instance.main_manager.0.private_ip}:2377"
    ]
  }

  depends_on = [aws_instance.main_manager]
}

resource "aws_instance" "worker" {
  count = var.cluster_worker_count
  ami = "******"
  instance_type = var.t2_micro
  subnet_id = aws_subnet.sandbox-public-subnet-1.id
  vpc_security_group_ids = [aws_security_group.sandbox-worker-security-group.id]
  key_name = aws_key_pair.ssh_key.id

  connection {
    type = var.connection_type
    host = self.public_ip
    user = var.user
    private_key = tls_private_key.global_key.private_key_pem
  }

  provisioner "remote-exec" {
    inline = [
//      "sudo docker swarm join ${aws_instance.main_manager.0.private_ip}:2377 --token $(sudo docker -H ${aws_instance.main_manager.0.private_ip} swarm join-token -q worker)",
      "sudo docker swarm join --token $(cat worker-token.txt) ${aws_instance.main_manager.0.private_ip}:2377"
    ]
  }

  depends_on = [aws_instance.main_manager]
}