resource "cloudflare_record" "domain_record" {
  name = var.domain
  type = var.record_type
  zone_id = var.zone_id
  value = aws_instance.main_manager.public_ip
  proxied = false
}

resource "cloudflare_record" "wildcard_record" {
  name = var.wildcard
  type = var.record_type
  zone_id = var.zone_id
  value = aws_instance.main_manager.public_ip
  proxied = false
}

resource "cloudflare_record" "www" {
  name = var.www
  type = var.record_type
  zone_id = var.zone_id
  value = aws_instance.main_manager.public_ip
  proxied = false
}

variable "email" {
  type = string
  description = "Cloudflare email"
}

variable "api_token" {
  type = string
  description = "Cloudflare token"
}

variable "domain" {
  type = string
  description = "Domain name"
}

variable "zone_id" {
  type = string
  description = "Cloudflare Zone ID"
}

variable "record_type" {
  type = string
  description = "Record type"
  default = "A"
}

variable "wildcard" {
  type = string
  description = "Wildcard name"
  default = "*"
}

variable "www" {
  type = string
  description = "www"
  default = "www"
}