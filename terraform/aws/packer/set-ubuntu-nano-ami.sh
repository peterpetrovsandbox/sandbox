#!/bin/bash
sudo su
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
sudo systemctl enable docker
sudo timedatectl set-timezone America/New_York
sudo apt install -y ntp
sudo systemctl start ntp
sudo systemctl enable ntp
sudo sysctl -w vm.max_map_count=262144
sudo echo vm.max_map_count=262144 >> /etc/sysctl.conf
sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo cp /etc/fstab /etc/fstab.bak
sudo echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
sudo docker plugin install --grant-all-permissions rexray/ebs:0.11.4 EBS_ACCESSKEY=******* EBS_SECRETKEY=******* EBS_REGION=us-east-1