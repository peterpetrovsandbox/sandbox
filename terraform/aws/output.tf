output "vpc_id" {
  value = aws_vpc.sandbox-vpc.id
}

output "vpc_cidr" {
  value = cidrhost(aws_vpc.sandbox-vpc.cidr_block,1)
}

output "vpc_subnet" {
  value = aws_subnet.sandbox-public-subnet-1
}

output "vpc_gateway" {
  value = aws_internet_gateway.sandbox-internet-gateway
}

output "vpc_route_table" {
  value = aws_route_table.sandbox-public-route-table
}

output "manager_security_group" {
  value = aws_security_group.sandbox-manager-security-group
}

output "worker_security_group" {
  value = aws_security_group.sandbox-worker-security-group
}

output "swarm_main_manager" {
  value = concat(aws_instance.main_manager.*.public_dns)
}

output "mount_target_dns" {
  value = aws_efs_mount_target.efs_mount_target.dns_name
}

output "swarm_managers" {
  value = concat(aws_instance.manager.*.public_dns)
}

output "domain_record" {
  value = cloudflare_record.domain_record
}

output "wildcard_record" {
  value = cloudflare_record.wildcard_record
}

output "www" {
  value = cloudflare_record.www
}
