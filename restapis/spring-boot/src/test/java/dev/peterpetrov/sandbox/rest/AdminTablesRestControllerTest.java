package dev.peterpetrov.sandbox.rest;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.AdminLog;
import dev.peterpetrov.sandbox.service.AdminTablesService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AdminTablesRestControllerTest {

    private AdminTablesRestController adminTablesRestController;
    private AdminTablesService adminTablesService;

    @BeforeEach
    void setUp() {
        adminTablesService = mock(AdminTablesService.class);
        adminTablesRestController = new AdminTablesRestController(adminTablesService);
    }

    @Test
    void usersFullOrder() {
        when(adminTablesService.usersFullOrder()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getUsersFullOrderRow()));
        ResponseEntity<List<UsersFullOrderRow>> responseEntity = adminTablesRestController.usersFullOrder();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getUsersFullOrderRow());
    }

    @Test
    void usersTotals() {
        when(adminTablesService.usersTotals()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getUsersTotals()));
        ResponseEntity<List<UsernameTotal>> responseEntity = adminTablesRestController.usersTotals();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getUsersTotals());
    }

    @Test
    void buyerFullOrder() {
        when(adminTablesService.buyerFullOrder()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getBuyerFullOrder()));
        ResponseEntity<List<BuyerFullOrderRow>> responseEntity = adminTablesRestController.buyerFullOrder();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getBuyerFullOrder());
    }

    @Test
    void buyerOrderByProductCode() {
        when(adminTablesService.buyerOrderByProductCode()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getBuyerOrderByProductCodeRow()));
        ResponseEntity<List<BuyerOrderByProductCodeRow>> responseEntity = adminTablesRestController.buyerOrderByProductCode();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getBuyerOrderByProductCodeRow());
    }

    @Test
    void resetOrder() {
        when(adminTablesService.clearOrder()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = adminTablesRestController.resetOrder();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void adminLog() {
        when(adminTablesService.adminLog()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getAdminLog()));
        ResponseEntity<List<AdminLog>> responseEntity = adminTablesRestController.adminLog();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getAdminLog());
    }

    private List<UsersFullOrderRow> getUsersFullOrderRow() {
        return new ArrayList<>(List.of(new UsersFullOrderRow(
                1, "2", "Full Sack", "user", "1001", "2-Row",
                "50", "Paid", "1", "10", "10"
        )));
    }

    private List<UsernameTotal> getUsersTotals() {
        return new ArrayList<>(List.of(new UsernameTotal("user", "10")));
    }

    private List<BuyerFullOrderRow> getBuyerFullOrder() {
        return new ArrayList<>(List.of(new BuyerFullOrderRow(
                1, "2", "Split", "1001", "2-Row", "50", "CLOSED"
        )));
    }

    private List<BuyerOrderByProductCodeRow> getBuyerOrderByProductCodeRow() {
        return new ArrayList<>(List.of(new BuyerOrderByProductCodeRow(
                "1001", "2-Row", 50.00, 3, 150.00
        )));
    }

    private List<AdminLog> getAdminLog() {
        AdminLog adminLog = new AdminLog();
        adminLog.setId(1);
        adminLog.setTimestamp("17:04:22 EST");
        adminLog.setException("IOException");
        return new ArrayList<>(List.of(adminLog));
    }
}