package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsernameTest {

    @Test
    void getName() {
        Username username = new Username();
        username.setName("user");
        assertThat(username.getName()).isEqualTo("user");
    }

    @Test
    void allArgsConstructor() {
        Username username = new Username("user");
        assertThat(username.getName()).isEqualTo("user");
    }
}