package dev.peterpetrov.sandbox.rest.exceptionhandler;

import dev.peterpetrov.sandbox.dto.ExceptionMessage;
import dev.peterpetrov.sandbox.entity.AdminLog;
import dev.peterpetrov.sandbox.exception.BuyClosedException;
import dev.peterpetrov.sandbox.exception.FullSacksException;
import dev.peterpetrov.sandbox.exception.RandomException;
import dev.peterpetrov.sandbox.exception.SplitsException;
import dev.peterpetrov.sandbox.repository.AdminLogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.ResultSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RestExceptionHandlerTest {

    private AdminLogRepository adminLogRepository;
    private RestExceptionHandler restExceptionHandler;

    @BeforeEach
    void setUp() {
        adminLogRepository = mock(AdminLogRepository.class);
        restExceptionHandler = new RestExceptionHandler(adminLogRepository);
    }

    @Test
    void handleBuyClosedException() {
        ResponseEntity<ExceptionMessage> responseEntity = restExceptionHandler.handleBuyClosedException(new BuyClosedException("Buy closed"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        ExceptionMessage exceptionMessage = responseEntity.getBody();
        assertThat(exceptionMessage.getResponse()).isEqualTo("Buy closed");
    }

    @Test
    void handleFullSacksException() {
        ResponseEntity<ExceptionMessage> responseEntity = restExceptionHandler.handleFullSacksException(new FullSacksException("No such sack id"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        ExceptionMessage exceptionMessage = responseEntity.getBody();
        assertThat(exceptionMessage.getResponse()).isEqualTo("No such sack id");
    }

    @Test
    void handleSplitsException() {
        ResponseEntity<ExceptionMessage> responseEntity = restExceptionHandler.handleSplitsException(new SplitsException("No such split id"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        ExceptionMessage exceptionMessage = responseEntity.getBody();
        assertThat(exceptionMessage.getResponse()).isEqualTo("No such split id");
    }

    @Test
    void handleRandomException() {
        ResponseEntity<ExceptionMessage> responseEntity = restExceptionHandler.handleRandomException(new RandomException("No such product code"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(responseEntity.getBody()).isNotNull();
        ExceptionMessage exceptionMessage = responseEntity.getBody();
        assertThat(exceptionMessage.getResponse()).isEqualTo("No such product code");
    }

    @Test
    void handleUnexpectedException() {
        when(adminLogRepository.save(any(AdminLog.class))).thenReturn(new AdminLog());
        ResponseEntity<ExceptionMessage> responseEntity = restExceptionHandler.handleUnexpectedException(new Exception());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(responseEntity.getBody()).isNotNull();
        ExceptionMessage exceptionMessage = responseEntity.getBody();
        assertThat(exceptionMessage.getResponse()).isEqualTo("Unexpected error when executing request");
    }
}