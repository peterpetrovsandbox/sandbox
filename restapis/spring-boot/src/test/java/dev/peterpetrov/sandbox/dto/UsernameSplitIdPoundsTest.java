package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsernameSplitIdPoundsTest {

    @Test
    void getSplitId() {
        UsernameSplitIdPounds usernameSplitIdPounds = new UsernameSplitIdPounds();
        usernameSplitIdPounds.setSplitId("1");
        assertThat(usernameSplitIdPounds.getSplitId()).isEqualTo("1");
    }

    @Test
    void allArgsConstructor() {
        UsernameSplitIdPounds usernameSplitIdPounds = new UsernameSplitIdPounds("user", "2", "5");
        assertThat(usernameSplitIdPounds.getUsername()).isEqualTo("user");
        assertThat(usernameSplitIdPounds.getSplitId()).isEqualTo("2");
        assertThat(usernameSplitIdPounds.getPounds()).isEqualTo("5");
    }
}