package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BuyerFullOrderRowTest {

    @Test
    void getId() {
        BuyerFullOrderRow buyerFullOrderRow = new BuyerFullOrderRow();
        buyerFullOrderRow.setId(1);
        assertThat(buyerFullOrderRow.getId()).isEqualTo(1);
    }

    @Test
    void allArgsConstructor() {
        BuyerFullOrderRow buyerFullOrderRow =
                new BuyerFullOrderRow(1, "2", "Full Sack", "1001", "2-Row", "50", "Paid");
        assertThat(buyerFullOrderRow.getId()).isEqualTo(1);
        assertThat(buyerFullOrderRow.getTableId()).isEqualTo("2");
        assertThat(buyerFullOrderRow.getType()).isEqualTo("Full Sack");
        assertThat(buyerFullOrderRow.getProductCode()).isEqualTo("1001");
        assertThat(buyerFullOrderRow.getProductDescription()).isEqualTo("2-Row");
        assertThat(buyerFullOrderRow.getProductPrice()).isEqualTo("50");
        assertThat(buyerFullOrderRow.getStatus()).isEqualTo("Paid");
    }
}