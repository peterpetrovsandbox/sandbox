package dev.peterpetrov.sandbox.service;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.AdminLog;
import dev.peterpetrov.sandbox.entity.FullSack;
import dev.peterpetrov.sandbox.entity.Split;
import dev.peterpetrov.sandbox.entity.UserSplit;
import dev.peterpetrov.sandbox.repository.AdminLogRepository;
import dev.peterpetrov.sandbox.repository.FullSacksRepository;
import dev.peterpetrov.sandbox.repository.SplitsRepository;
import dev.peterpetrov.sandbox.repository.UserSplitsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class AdminTablesServiceTest {

    private FullSacksRepository fullSacksRepository;
    private UserSplitsRepository userSplitsRepository;
    private SplitsRepository splitsRepository;
    private AdminLogRepository adminLogRepository;
    private EntityManager entityManager;
    private AdminTablesService adminTablesService;

    @BeforeEach
    void setUp() {
        fullSacksRepository = mock(FullSacksRepository.class);
        userSplitsRepository = mock(UserSplitsRepository.class);
        splitsRepository = mock(SplitsRepository.class);
        adminLogRepository = mock(AdminLogRepository.class);
        entityManager = mock(EntityManager.class);
        adminTablesService = new AdminTablesService(fullSacksRepository, userSplitsRepository, splitsRepository, adminLogRepository, entityManager);
    }

    @Test
    void usersFullOrder() {
        when(fullSacksRepository.findAll()).thenReturn(List.of(getFullSack(), getFullSack(), getFullSack()));
        when(userSplitsRepository.findByStatus(anyString())).thenReturn(List.of(getUserSplit1(), getUserSplit1()));

        ResponseEntity<List<UsersFullOrderRow>> responseEntity = adminTablesService.usersFullOrder();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(5);
        verify(fullSacksRepository, times(1)).findAll();
        verify(userSplitsRepository, times(1)).findByStatus(anyString());
    }

    @Test
    void usersTotals() {
        when(fullSacksRepository.findAll()).thenReturn(List.of(getFullSack(), getFullSack()));
        when(userSplitsRepository.findByStatus(anyString())).thenReturn(List.of(getUserSplit1(), getUserSplit1(), getUserSplit2()));
        when(fullSacksRepository.findAllByUsername(anyString())).thenReturn(List.of(getFullSack(), getFullSack())).thenReturn(new ArrayList<>());
        when(userSplitsRepository.findAllByUsernameAndStatus(anyString(), anyString())).thenReturn(List.of(getUserSplit1())).thenReturn(List.of(getUserSplit2()));

        ResponseEntity<List<UsernameTotal>> responseEntity = adminTablesService.usersTotals();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        Map<String, UsernameTotal> usersTotals = responseEntity.getBody().stream().collect(Collectors.toMap(UsernameTotal::getUsername, usernameTotal -> usernameTotal));
        assertThat(usersTotals.get("user1").getTotal()).isEqualTo("120.0");
        assertThat(usersTotals.get("user2").getTotal()).isEqualTo("20.0");
        verify(fullSacksRepository, times(1)).findAll();
        verify(userSplitsRepository, times(1)).findByStatus(anyString());
        verify(fullSacksRepository, times(2)).findAllByUsername(anyString());
        verify(userSplitsRepository, times(2)).findAllByUsernameAndStatus(anyString(), anyString());
    }

    @Test
    void buyerFullOrder() {
        when(fullSacksRepository.findAll()).thenReturn(List.of(getFullSack(), getFullSack(), getFullSack()));
        when(splitsRepository.findAllByStatus(anyString())).thenReturn(List.of(getSplit(), getSplit()));

        ResponseEntity<List<BuyerFullOrderRow>> responseEntity = adminTablesService.buyerFullOrder();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(5);
    }

    @Test
    void buyerOrderByProductCode() {
        when(fullSacksRepository.findAll()).thenReturn(List.of(getFullSack(), getFullSack()));
        when(splitsRepository.findAllByStatus(anyString())).thenReturn(List.of(getSplit(), getSplit(), getSplit()));
        when(fullSacksRepository.findAllByProductCode(anyString())).thenReturn(List.of(getFullSack(), getFullSack()));
        when(splitsRepository.findAllByProductCodeAndStatus(anyString(), anyString())).thenReturn(List.of(getSplit(), getSplit(), getSplit()));

        ResponseEntity<List<BuyerOrderByProductCodeRow>> responseEntity = adminTablesService.buyerOrderByProductCode();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(1);
        assertThat(responseEntity.getBody().get(0).getQuantity()).isEqualTo(5);
        assertThat(responseEntity.getBody().get(0).getTotal()).isEqualTo(250.0);
        assertThat(responseEntity.getBody().get(0).getProductCode()).isEqualTo("1600C");
        assertThat(responseEntity.getBody().get(0).getProductDescription()).isEqualTo("2-Row");
        verify(fullSacksRepository, times(1)).findAll();
        verify(splitsRepository, times(1)).findAllByStatus(anyString());
        verify(fullSacksRepository, times(1)).findAllByProductCode(anyString());
        verify(splitsRepository, times(1)).findAllByProductCodeAndStatus(anyString(), anyString());
    }

    @Test
    void buyerOrderByProductCodeFullSacksEmpty() {
        when(fullSacksRepository.findAll()).thenReturn(new ArrayList<>());
        when(splitsRepository.findAllByStatus(anyString())).thenReturn(List.of(getSplit(), getSplit(), getSplit()));
        when(fullSacksRepository.findAllByProductCode(anyString())).thenReturn(new ArrayList<>());
        when(splitsRepository.findAllByProductCodeAndStatus(anyString(), anyString())).thenReturn(List.of(getSplit(), getSplit(), getSplit()));

        ResponseEntity<List<BuyerOrderByProductCodeRow>> responseEntity = adminTablesService.buyerOrderByProductCode();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(1);
        assertThat(responseEntity.getBody().get(0).getQuantity()).isEqualTo(3);
        assertThat(responseEntity.getBody().get(0).getTotal()).isEqualTo(150.0);
        assertThat(responseEntity.getBody().get(0).getProductCode()).isEqualTo("1600C");
        assertThat(responseEntity.getBody().get(0).getProductDescription()).isEqualTo("2-Row");
        verify(fullSacksRepository, times(1)).findAll();
        verify(splitsRepository, times(1)).findAllByStatus(anyString());
        verify(fullSacksRepository, times(1)).findAllByProductCode(anyString());
        verify(splitsRepository, times(1)).findAllByProductCodeAndStatus(anyString(), anyString());
    }

    @Test
    void adminLog() {
        when(adminLogRepository.findAll()).thenReturn(List.of(new AdminLog(), new AdminLog()));

        ResponseEntity<List<AdminLog>> responseEntity = adminTablesService.adminLog();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody().size()).isEqualTo(2);
        verify(adminLogRepository, times(1)).findAll();
    }

    @Test
    void clearOrder() {
        Query query = mock(Query.class);
        when(entityManager.createNativeQuery(anyString())).thenReturn(query);
        when(query.executeUpdate()).thenReturn(1);

        ResponseEntity<Success> responseEntity = adminTablesService.clearOrder();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Order cleared successful"));
        verify(entityManager, times(5)).createNativeQuery(anyString());
        verify(query, times(5)).executeUpdate();
    }

    private FullSack getFullSack() {
        FullSack fullSack = new FullSack();
        fullSack.setId(1);
        fullSack.setUsername("user1");
        fullSack.setProductCode("1600C");
        fullSack.setTotalWeight("55");
        fullSack.setProductPrice("50");
        fullSack.setPaid("YES");
        fullSack.setProductDescription("2-Row");
        return fullSack;
    }

    private UserSplit getUserSplit1() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setProductCode("1600C");
        userSplit.setProductDescription("2-Row");
        userSplit.setProductPrice("50");
        userSplit.setTotalWeight("50");
        userSplit.setStartedBy("user1");
        userSplit.setPricePerPound("1");
        userSplit.setClaimed("20");
        userSplit.setStatus("CLOSED");
        userSplit.setPaid("NO");
        userSplit.setSplitId("1");
        userSplit.setUsername("user1");
        userSplit.setSplitTotal("20");
        return userSplit;
    }

    private UserSplit getUserSplit2() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setProductCode("1600C");
        userSplit.setProductDescription("2-Row");
        userSplit.setProductPrice("50");
        userSplit.setTotalWeight("50");
        userSplit.setStartedBy("user2");
        userSplit.setPricePerPound("1");
        userSplit.setClaimed("20");
        userSplit.setStatus("CLOSED");
        userSplit.setPaid("NO");
        userSplit.setSplitId("1");
        userSplit.setUsername("user2");
        userSplit.setSplitTotal("20");
        return userSplit;
    }

    private Split getSplit() {
        Split split = new Split();
        split.setId(1);
        split.setProductCode("1600C");
        split.setProductDescription("2-Row");
        split.setProductPrice("50");
        split.setTotalWeight("50");
        split.setAvailable("0");
        split.setLocked("NO");
        split.setStartedBy("user");
        split.setPricePerPound("1");
        split.setClaimed("50");
        split.setStatus("CLOSED");
        split.setPaid("NO");
        return split;
    }
}