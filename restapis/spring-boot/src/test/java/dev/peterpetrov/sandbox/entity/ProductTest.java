package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ProductTest {

    @Test
    void allFields() {
        Product product = new Product();
        product.setId(1);
        product.setProductCode("1001");
        product.setProductDescription("2-Row");
        product.setProductPrice("50");
        product.setTotalWeight("55");
        product.setBaseMalt("YES");
        assertThat(product.getId()).isEqualTo(1);
        assertThat(product.getProductCode()).isEqualTo("1001");
        assertThat(product.getProductDescription()).isEqualTo("2-Row");
        assertThat(product.getProductPrice()).isEqualTo("50");
        assertThat(product.getTotalWeight()).isEqualTo("55");
        assertThat(product.getBaseMalt()).isEqualTo("YES");
    }
}