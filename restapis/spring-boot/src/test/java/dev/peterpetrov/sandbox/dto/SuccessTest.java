package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SuccessTest {

    @Test
    void getResponse() {
        Success success = new Success();
        success.setResponse("Success");
        assertThat(success.getResponse()).isEqualTo("Success");
    }

    @Test
    void allArgsConstructor() {
        Success success = new Success("Success");
        assertThat(success.getResponse()).isEqualTo("Success");
    }
}