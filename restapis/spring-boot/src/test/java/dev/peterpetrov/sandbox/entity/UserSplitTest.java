package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UserSplitTest {

    @Test
    void getSplitId() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setProductCode("1001");
        userSplit.setProductDescription("2-Row");
        userSplit.setProductPrice("50");
        userSplit.setTotalWeight("55");
        userSplit.setPaid("YES");
        userSplit.setStartedBy("user");
        userSplit.setPricePerPound("1.50");
        userSplit.setClaimed("10");
        userSplit.setStatus("OPEN");
        userSplit.setSplitId("2");
        userSplit.setUsername("user");
        userSplit.setSplitTotal("15");
        assertThat(userSplit.getId()).isEqualTo(1);
        assertThat(userSplit.getProductCode()).isEqualTo("1001");
        assertThat(userSplit.getProductDescription()).isEqualTo("2-Row");
        assertThat(userSplit.getProductPrice()).isEqualTo("50");
        assertThat(userSplit.getTotalWeight()).isEqualTo("55");
        assertThat(userSplit.getPaid()).isEqualTo("YES");
        assertThat(userSplit.getStartedBy()).isEqualTo("user");
        assertThat(userSplit.getPricePerPound()).isEqualTo("1.50");
        assertThat(userSplit.getClaimed()).isEqualTo("10");
        assertThat(userSplit.getStatus()).isEqualTo("OPEN");
        assertThat(userSplit.getSplitId()).isEqualTo("2");
        assertThat(userSplit.getUsername()).isEqualTo("user");
        assertThat(userSplit.getSplitTotal()).isEqualTo("15");
    }
}