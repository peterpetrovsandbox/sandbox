package dev.peterpetrov.sandbox.service;

import dev.peterpetrov.sandbox.dto.Message;
import dev.peterpetrov.sandbox.dto.ProductCodePrice;
import dev.peterpetrov.sandbox.dto.Success;
import dev.peterpetrov.sandbox.entity.*;
import dev.peterpetrov.sandbox.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class RandomServiceTest {

    private RandomRepository randomRepository;
    private FullSacksRepository fullSacksRepository;
    private SplitsRepository splitsRepository;
    private UserLogRepository userLogRepository;
    private UserSplitsRepository userSplitsRepository;
    private ProductRepository productRepository;
    private RandomService randomService;

    @BeforeEach
    void setUp() {
        randomRepository = mock(RandomRepository.class);
        fullSacksRepository = mock(FullSacksRepository.class);
        splitsRepository = mock(SplitsRepository.class);
        userLogRepository = mock(UserLogRepository.class);
        userSplitsRepository = mock(UserSplitsRepository.class);
        productRepository = mock(ProductRepository.class);
        randomService = new RandomService(randomRepository, fullSacksRepository, productRepository, splitsRepository, userLogRepository, userSplitsRepository);
    }

    @Test
    void openCloseBuySetToClose() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomBuyStatusOpen());
        when(randomRepository.save(any(Random.class))).thenReturn(new Random());

        ResponseEntity<Success> responseEntity = randomService.openCloseBuy();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Buy closed successful"));
        verify(randomRepository, times(1)).findById(anyInt());
        verify(randomRepository, times(1)).save(any(Random.class));
    }

    @Test
    void openCloseBuySetToOpen() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomBuyStatusClosed());
        when(randomRepository.save(any(Random.class))).thenReturn(new Random());

        ResponseEntity<Success> responseEntity = randomService.openCloseBuy();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Buy open successful"));
        verify(randomRepository, times(1)).findById(anyInt());
        verify(randomRepository, times(1)).save(any(Random.class));
    }

    @Test
    void updateMessage() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomBuyMessage());
        when(randomRepository.save(any(Random.class))).thenReturn(new Random());

        ResponseEntity<Success> responseEntity = randomService.updateMessage(new Message());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("New message set successful"));
        verify(randomRepository, times(1)).findById(anyInt());
        verify(randomRepository, times(1)).save(any(Random.class));
    }

    @Test
    void currentOrderCount() {
        when(fullSacksRepository.findAll()).thenReturn(List.of(new FullSack(), new FullSack()));
        when(splitsRepository.findAllByStatus(anyString())).thenReturn(List.of(new Split(), new Split(), new Split()));

        ResponseEntity<Message> responseEntity = randomService.currentOrderCount();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("5"));
        verify(fullSacksRepository, times(1)).findAll();
        verify(splitsRepository, times(1)).findAllByStatus(anyString());
    }

    @Test
    void getUserLog() {
        when(userLogRepository.findAll()).thenReturn(getUserLogData());

        ResponseEntity<List<UserLog>> responseEntity = randomService.getUserLog();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getUserLogData());
        verify(userLogRepository, times(1)).findAll();
    }

    @Test
    void getBuyStatus() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomBuyStatusClosed());

        ResponseEntity<Message> responseEntity = randomService.getBuyStatus();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("Closed"));
        verify(randomRepository, times(1)).findById(anyInt());
    }

    @Test
    void getMessage() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomBuyMessage());

        ResponseEntity<Message> responseEntity = randomService.getMessage();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("Buy will close on May 27th"));
        verify(randomRepository, times(1)).findById(anyInt());
    }

    @Test
    void getUserTotal() {
        when(fullSacksRepository.findAllByUsername(anyString())).thenReturn(List.of(getFullSack()));
        when(userSplitsRepository.findAllByUsernameAndStatus(anyString(), anyString())).thenReturn(List.of(getUserSplit()));

        ResponseEntity<Message> responseEntity = randomService.getUserTotal("user");
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("66.90"));
        verify(fullSacksRepository, times(1)).findAllByUsername(anyString());
        verify(userSplitsRepository, times(1)).findAllByUsernameAndStatus(anyString(), anyString());
    }

    @Test
    void setPrice() {
        when(productRepository.findByProductCode(anyString())).thenReturn(getProduct());
        when(productRepository.save(any(Product.class))).thenReturn(new Product());

        ResponseEntity<Success> responseEntity = randomService.setPrice(new ProductCodePrice("1600C", "55.10"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Product price set successfully"));
        verify(productRepository, times(1)).findByProductCode(anyString());
    }

    @Test
    void setPriceNoSuchProductCode() {
        when(productRepository.findByProductCode(anyString())).thenReturn(null);

        assertThatThrownBy(() -> randomService.setPrice(new ProductCodePrice("1600C", "55.10")))
                .hasNoCause()
                .hasMessage("No such product code");
    }

    @Test
    void orderTotal() {
        when(fullSacksRepository.findAll()).thenReturn(List.of(getFullSack(), getFullSack()));
        when(splitsRepository.findAllByStatus(anyString())).thenReturn(List.of(getSplit(), getSplit(), getSplit()));

        ResponseEntity<Message> responseEntity = randomService.orderTotal();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("251.68"));
    }

    private Random getRandomBuyStatusOpen() {
        Random random = new Random();
        random.setId(1);
        random.setUserAction("Open");
        return random;
    }

    private Random getRandomBuyStatusClosed() {
        Random random = new Random();
        random.setId(1);
        random.setUserAction("Closed");
        return random;
    }

    private Random getRandomBuyMessage() {
        Random random = new Random();
        random.setId(2);
        random.setUserAction("Buy will close on May 27th");
        return random;
    }

    private List<UserLog> getUserLogData() {
        UserLog userLog = new UserLog();
        userLog.setId(1);
        userLog.setUsername("user");
        userLog.setUserAction("Added full sack");
        userLog.setTimestamp("17:31:05 EST");
        return List.of(userLog);
    }

    private FullSack getFullSack() {
        FullSack fullSack = new FullSack();
        fullSack.setId(1);
        fullSack.setUsername("user");
        fullSack.setProductCode("1600C");
        fullSack.setTotalWeight("55");
        fullSack.setProductPrice("50.70");
        fullSack.setPaid("YES");
        return fullSack;
    }

    private UserSplit getUserSplit() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setProductCode("1600C");
        userSplit.setProductDescription("2-Row");
        userSplit.setProductPrice("50");
        userSplit.setTotalWeight("50");
        userSplit.setStartedBy("user");
        userSplit.setPricePerPound("0.81");
        userSplit.setClaimed("20");
        userSplit.setStatus("CLOSED");
        userSplit.setPaid("NO");
        userSplit.setSplitId("1");
        userSplit.setUsername("user");
        userSplit.setSplitTotal("16.20");
        return userSplit;
    }

    private Product getProduct() {
        Product product = new Product();
        product.setId(1);
        product.setProductPrice("50");
        product.setProductCode("1600C");
        product.setProductDescription("2-Row");
        product.setBaseMalt("YES");
        product.setTotalWeight("55");
        return product;
    }

    private Split getSplit() {
        Split split = new Split();
        split.setId(1);
        split.setProductCode("1600C");
        split.setProductDescription("2-Row");
        split.setProductPrice("50.09");
        split.setTotalWeight("50");
        split.setAvailable("30");
        split.setLocked("NO");
        split.setStartedBy("user");
        split.setPricePerPound("1");
        split.setClaimed("20");
        split.setStatus("OPEN");
        split.setPaid("NO");
        return split;
    }
}