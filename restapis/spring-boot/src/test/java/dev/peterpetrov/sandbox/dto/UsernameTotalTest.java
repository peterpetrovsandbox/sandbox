package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsernameTotalTest {

    @Test
    void getTotal() {
        UsernameTotal usernameTotal = new UsernameTotal();
        usernameTotal.setTotal("10");
        assertThat(usernameTotal.getTotal()).isEqualTo("10");
    }

    @Test
    void allArgsConstructor() {
        UsernameTotal usernameTotal = new UsernameTotal("user", "10");
        assertThat(usernameTotal.getUsername()).isEqualTo("user");
        assertThat(usernameTotal.getTotal()).isEqualTo("10");
    }
}