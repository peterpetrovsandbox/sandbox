package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class RandomTest {

    private Random random;

    @BeforeEach
    void setUp() {
        random = new Random();
    }

    @Test
    void getId() {
        random.setId(1);
        assertThat(random.getId()).isEqualTo(1);
    }

    @Test
    void getUserAction() {
        random.setUserAction("insert");
        assertThat(random.getUserAction()).isEqualTo("insert");
    }
}