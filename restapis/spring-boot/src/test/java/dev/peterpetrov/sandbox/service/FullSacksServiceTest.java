package dev.peterpetrov.sandbox.service;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.FullSack;
import dev.peterpetrov.sandbox.entity.Product;
import dev.peterpetrov.sandbox.entity.UserLog;
import dev.peterpetrov.sandbox.repository.FullSacksRepository;
import dev.peterpetrov.sandbox.repository.ProductRepository;
import dev.peterpetrov.sandbox.repository.UserLogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

class FullSacksServiceTest {

    private FullSacksService fullSacksService;
    private FullSacksRepository fullSacksRepository;
    private ProductRepository productRepository;
    private UserLogRepository userLogRepository;
    private UtilitiesService utilitiesService;


    @BeforeEach
    void setUp() {
        fullSacksRepository = mock(FullSacksRepository.class);
        productRepository = mock(ProductRepository.class);
        userLogRepository = mock(UserLogRepository.class);
        utilitiesService = mock(UtilitiesService.class);
        fullSacksService = new FullSacksService(fullSacksRepository, productRepository, userLogRepository, utilitiesService);
    }

    @Test
    void insertFullSackThrowsFullSacksException() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(productRepository.findByProductCode(anyString())).thenReturn(null);
        assertThatThrownBy(() -> fullSacksService.insertFullSack(new UsernameProductCode()))
                .hasNoCause()
                .hasMessage("No such product code");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(productRepository, times(1)).findByProductCode(null);
    }

    @Test
    void insertFullSack() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(productRepository.findByProductCode(anyString())).thenReturn(getProduct());
        when(fullSacksRepository.save(any())).thenReturn(new FullSack());
        when(userLogRepository.save(any())).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = fullSacksService.insertFullSack(new UsernameProductCode("user", "1600C"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Order full sack successful"));
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(productRepository, times(1)).findByProductCode(anyString());
        verify(fullSacksRepository, times(1)).save(any());
        verify(userLogRepository, times(1)).save(any());
    }

    @Test
    void removeFullSackThrowsFullSackException() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(fullSacksRepository.findFullSacksById(anyInt())).thenReturn(null);

        assertThatThrownBy(() -> fullSacksService.removeFullSack(new UsernameSackId()))
                .hasNoCause()
                .hasMessage("No such sack id");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(fullSacksRepository, times(1)).findFullSacksById(anyInt());
    }

    @Test
    void removeFullSack() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(fullSacksRepository.findFullSacksById(anyInt())).thenReturn(new FullSack());
        when(userLogRepository.save(any())).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = fullSacksService.removeFullSack(new UsernameSackId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Remove full sack successful"));
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(fullSacksRepository, times(1)).findFullSacksById(anyInt());
        verify(userLogRepository, times(1)).save(any());
    }

    @Test
    void adminRemoveSackThrowsFullSackException() {
        when(fullSacksRepository.findFullSacksById(anyInt())).thenReturn(null);

        assertThatThrownBy(() -> fullSacksService.adminRemoveSack(new TableId("1")))
                .hasNoCause()
                .hasMessage("No such table id");
        verify(fullSacksRepository, times(1)).findFullSacksById(anyInt());
    }

    @Test
    void adminRemoveSack() {
        when(fullSacksRepository.findFullSacksById(anyInt())).thenReturn(new FullSack());
        doNothing().when(fullSacksRepository).delete(any(FullSack.class));
        when(userLogRepository.save(any())).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = fullSacksService.adminRemoveSack(new TableId("1"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Sack removed successful"));
        verify(fullSacksRepository, times(1)).findFullSacksById(anyInt());
        verify(fullSacksRepository, times(1)).delete(any(FullSack.class));
        verify(userLogRepository, times(1)).save(any());
    }

    @Test
    void myOrderFullSacksEmpty() {
        when(fullSacksRepository.findAllByUsername(anyString())).thenReturn(new ArrayList<>());

        ResponseEntity<List<FullSack>> responseEntity = fullSacksService.myOrderFullSacks(new Username("user"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new ArrayList<>());
        verify(fullSacksRepository, times(1)).findAllByUsername(anyString());
    }

    @Test
    void myOrderFullSacks() {
        when(fullSacksRepository.findAllByUsername(anyString())).thenReturn(new ArrayList<>(List.of(getFullSack())));

        ResponseEntity<List<FullSack>> responseEntity = fullSacksService.myOrderFullSacks(new Username("user"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new ArrayList<>(List.of(getFullSack())));
        verify(fullSacksRepository, times(1)).findAllByUsername(anyString());
    }

    @Test
    void getFullSacksEmpty() {
        when(fullSacksRepository.findAll()).thenReturn(new ArrayList<>());

        ResponseEntity<List<FullSack>> responseEntity = fullSacksService.getFullSacks();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new ArrayList<>());
        verify(fullSacksRepository, times(1)).findAll();
    }

    @Test
    void getFullSacks() {
        when(fullSacksRepository.findAll()).thenReturn(new ArrayList<>(List.of(getFullSack())));

        ResponseEntity<List<FullSack>> responseEntity = fullSacksService.getFullSacks();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new ArrayList<>(List.of(getFullSack())));
        verify(fullSacksRepository, times(1)).findAll();
    }

    private Product getProduct() {
        Product product = new Product();
        product.setId(1);
        product.setProductPrice("50");
        product.setProductCode("1600C");
        product.setProductDescription("2-Row");
        product.setBaseMalt("YES");
        product.setTotalWeight("55");
        return product;
    }

    private FullSack getFullSack() {
        FullSack fullSack = new FullSack();
        fullSack.setId(1);
        fullSack.setUsername("user");
        fullSack.setProductCode("1600C");
        fullSack.setTotalWeight("55");
        fullSack.setProductPrice("50");
        fullSack.setPaid("YES");
        return fullSack;
    }
}