package dev.peterpetrov.sandbox.service;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.Product;
import dev.peterpetrov.sandbox.entity.Split;
import dev.peterpetrov.sandbox.entity.UserLog;
import dev.peterpetrov.sandbox.entity.UserSplit;
import dev.peterpetrov.sandbox.repository.ProductRepository;
import dev.peterpetrov.sandbox.repository.SplitsRepository;
import dev.peterpetrov.sandbox.repository.UserLogRepository;
import dev.peterpetrov.sandbox.repository.UserSplitsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

class SplitsServiceTest {

    private SplitsRepository splitsRepository;
    private ProductRepository productRepository;
    private UserLogRepository userLogRepository;
    private UserSplitsRepository userSplitsRepository;
    private UtilitiesService utilitiesService;
    private SplitsService splitsService;

    @BeforeEach
    void setUp() {
        splitsRepository = mock(SplitsRepository.class);
        productRepository = mock(ProductRepository.class);
        userLogRepository = mock(UserLogRepository.class);
        userSplitsRepository = mock(UserSplitsRepository.class);
        utilitiesService = mock(UtilitiesService.class);
        splitsService = new SplitsService(splitsRepository, productRepository, userLogRepository, userSplitsRepository, utilitiesService);
    }

    @Test
    void startSplit() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(productRepository.findByProductCode(anyString())).thenReturn(getProductNotBaseMalt());
        when(splitsRepository.findSplitsByProductCodeAndStatus(anyString(), anyString())).thenReturn(null);
        when(splitsRepository.save(any(Split.class))).thenReturn(new Split());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.startSplit(new UsernameProductCode("user", "1600C"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Split started successful"));
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(productRepository, times(1)).findByProductCode(anyString());
        verify(splitsRepository, times(1)).save(any(Split.class));
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void startSplitNoSuchProductCode() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(productRepository.findByProductCode(anyString())).thenReturn(null);

        assertThatThrownBy(() -> splitsService.startSplit(new UsernameProductCode("user", "1600C")))
                .hasNoCause()
                .hasMessage("No such product code");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(productRepository, times(1)).findByProductCode(anyString());
    }

    @Test
    void startSplitBaseMalt() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(productRepository.findByProductCode(anyString())).thenReturn(getProductBaseMalt());

        assertThatThrownBy(() -> splitsService.startSplit(new UsernameProductCode("user", "1600C")))
                .hasNoCause()
                .hasMessage("Base malt, cannot start split");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(productRepository, times(1)).findByProductCode(anyString());
    }

    @Test
    void startSplitExistingSplitOpen() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(productRepository.findByProductCode(anyString())).thenReturn(getProductNotBaseMalt());
        when(splitsRepository.findSplitsByProductCodeAndStatus(anyString(), anyString())).thenReturn(new Split());

        assertThatThrownBy(() -> splitsService.startSplit(new UsernameProductCode("user", "1600C")))
                .hasNoCause()
                .hasMessage("There is an existing open split");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(productRepository, times(1)).findByProductCode(anyString());
        verify(splitsRepository, times(1)).findSplitsByProductCodeAndStatus(anyString(), anyString());
    }

    @Test
    void joinSplit() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(splitsRepository.findById(anyInt())).thenReturn(getSplit());
        when(userSplitsRepository.findBySplitIdAndUsername(anyString(), anyString())).thenReturn(null);
        when(userSplitsRepository.save(any(UserSplit.class))).thenReturn(new UserSplit());
        when(splitsRepository.save(any(Split.class))).thenReturn(new Split());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.joinSplit(new UsernameSplitIdPounds("user", "1", "30"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Split join successful"));
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(splitsRepository, times(1)).findById(anyInt());
        verify(userSplitsRepository, times(1)).findBySplitIdAndUsername(anyString(), anyString());
        verify(userSplitsRepository, times(1)).save(any(UserSplit.class));
        verify(splitsRepository, times(1)).save(any(Split.class));
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void joinSplitNoSuchSplitId() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(splitsRepository.findById(anyInt())).thenReturn(null);

        assertThatThrownBy(() -> splitsService.joinSplit(new UsernameSplitIdPounds("user", "1", "30")))
                .hasNoCause()
                .hasMessage("No such split id");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(splitsRepository, times(1)).findById(anyInt());
    }

    @Test
    void joinSplitClaimingMoreThanAvailable() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(splitsRepository.findById(anyInt())).thenReturn(getSplit());

        assertThatThrownBy(() -> splitsService.joinSplit(new UsernameSplitIdPounds("user", "1", "35")))
                .hasNoCause()
                .hasMessage("Claiming more than available");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(splitsRepository, times(1)).findById(anyInt());
    }

    @Test
    void joinSplitFivePoundIncrements() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(splitsRepository.findById(anyInt())).thenReturn(getSplit());

        assertThatThrownBy(() -> splitsService.joinSplit(new UsernameSplitIdPounds("user", "1", "4")))
                .hasNoCause()
                .hasMessage("Must order in 5 Lb increments");
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(splitsRepository, times(1)).findById(anyInt());
    }

    @Test
    void joinSplitExistingSplitOpen() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(splitsRepository.findById(anyInt())).thenReturn(getSplit());
        when(userSplitsRepository.findBySplitIdAndUsername(anyString(), anyString())).thenReturn(new UserSplit());
        when(userSplitsRepository.findBySplitIdAndUsername(anyString(), anyString())).thenReturn(getUserSplit());
        when(userSplitsRepository.save(any(UserSplit.class))).thenReturn(new UserSplit());
        when(splitsRepository.save(any(Split.class))).thenReturn(new Split());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.joinSplit(new UsernameSplitIdPounds("user", "1", "30"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Adding more to split successful"));
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(splitsRepository, times(1)).findById(anyInt());
        verify(userSplitsRepository, times(2)).findBySplitIdAndUsername(anyString(), anyString());
        verify(userSplitsRepository, times(1)).save(any(UserSplit.class));
        verify(splitsRepository, times(1)).save(any(Split.class));
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void removeSplitUser() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(userSplitsRepository.findBySplitIdAndUsername(anyString(), anyString())).thenReturn(getUserSplit());
        when(splitsRepository.findById(anyInt())).thenReturn(getSplit());
        when(userSplitsRepository.findAllBySplitId(anyString())).thenReturn(List.of(getUserSplit()));
        when(userSplitsRepository.save(any(UserSplit.class))).thenReturn(new UserSplit());
        doNothing().when(userSplitsRepository).delete(any(UserSplit.class));
        when(splitsRepository.save(any(Split.class))).thenReturn(new Split());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.removeSplitUser(new UsernameSplitId("user", "1"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Split remove successful"));
        verify(utilitiesService, times(1)).checkIfBuyIsOpen();
        verify(userSplitsRepository, times(1)).save(any(UserSplit.class));
        verify(userSplitsRepository, times(1)).findBySplitIdAndUsername(anyString(), anyString());
        verify(splitsRepository, times(1)).findById(anyInt());
        verify(userSplitsRepository, times(1)).delete(any(UserSplit.class));
        verify(splitsRepository, times(1)).save(any(Split.class));
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void removeSplitUserNoSuchSplitId() {
        doNothing().when(utilitiesService).checkIfBuyIsOpen();
        when(userSplitsRepository.findBySplitIdAndUsername(anyString(), anyString())).thenReturn(null);

        assertThatThrownBy(() -> splitsService.removeSplitUser(new UsernameSplitId("user", "1")))
                .hasNoCause()
                .hasMessage("No such split id");
    }

    @Test
    void removeUserSplitAdmin() {
        when(userSplitsRepository.findById(anyInt())).thenReturn(getUserSplit());
        when(splitsRepository.findById(anyInt())).thenReturn(getSplit());
        when(userSplitsRepository.findAllBySplitId(anyString())).thenReturn(List.of(getUserSplit()));
        when(userSplitsRepository.save(any(UserSplit.class))).thenReturn(new UserSplit());
        doNothing().when(userSplitsRepository).delete(any(UserSplit.class));
        when(splitsRepository.save(any(Split.class))).thenReturn(new Split());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.removeUserSplitAdmin(new SplitId("1"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Split remove successful"));
        verify(userSplitsRepository, times(1)).save(any(UserSplit.class));
        verify(splitsRepository, times(1)).findById(anyInt());
        verify(userSplitsRepository, times(1)).delete(any(UserSplit.class));
        verify(splitsRepository, times(1)).save(any(Split.class));
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void removeUserSplitAdminNoSuchTableId() {
        when(userSplitsRepository.findById(anyInt())).thenReturn(null);

        assertThatThrownBy(() -> splitsService.removeUserSplitAdmin(new SplitId("1")))
                .hasNoCause()
                .hasMessage("No such table id");
    }

    @Test
    void removeAllOpenSplits() {
        when(splitsRepository.findByStatus(anyString())).thenReturn(List.of(getSplit()));
        when(userSplitsRepository.findByStatus(anyString())).thenReturn(List.of(getUserSplit()));
        doNothing().when(splitsRepository).deleteInBatch(anyCollection());
        doNothing().when(userSplitsRepository).deleteInBatch(anyCollection());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.removeAllOpenSplits();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Open splits removal successful"));
        verify(splitsRepository, times(1)).findByStatus(anyString());
        verify(userSplitsRepository, times(1)).findByStatus(anyString());
        verify(splitsRepository, times(1)).deleteInBatch(anyCollection());
        verify(userSplitsRepository, times(1)).deleteInBatch(anyCollection());
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void removeFullSplit() {
        doNothing().when(splitsRepository).deleteById(anyInt());
        doNothing().when(userSplitsRepository).deleteAllBySplitId(anyString());
        when(userLogRepository.save(any(UserLog.class))).thenReturn(new UserLog());

        ResponseEntity<Success> responseEntity = splitsService.removeFullSplit(new TableId("1"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Split and corresponding user splits removed successfully"));
        verify(splitsRepository, times(1)).deleteById(anyInt());
        verify(userSplitsRepository, times(1)).deleteAllBySplitId(anyString());
        verify(userLogRepository, times(1)).save(any(UserLog.class));
    }

    @Test
    void myOrderSplits() {
        when(userSplitsRepository.findAllByUsername(anyString())).thenReturn(List.of(getUserSplit()));

        ResponseEntity<List<UserSplit>> responseEntity = splitsService.myOrderSplits(new Username("user"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(List.of(getUserSplit()));
        verify(userSplitsRepository, times(1)).findAllByUsername(anyString());
    }

    @Test
    void getSplits() {
        when(splitsRepository.findAll()).thenReturn(List.of(getSplit()));

        ResponseEntity<List<Split>> responseEntity = splitsService.getSplits();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(List.of(getSplit()));
        verify(splitsRepository, times(1)).findAll();
    }

    @Test
    void getUsersSplits() {
        when(userSplitsRepository.findAll()).thenReturn(List.of(getUserSplit()));

        ResponseEntity<List<UserSplit>> responseEntity = splitsService.getUsersSplits();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(List.of(getUserSplit()));
        verify(userSplitsRepository, times(1)).findAll();
    }

    private Product getProductNotBaseMalt() {
        Product product = new Product();
         product.setId(1);
         product.setProductCode("1600C");
         product.setProductDescription("2-Row");
         product.setProductPrice("50");
         product.setTotalWeight("55");
         product.setBaseMalt("NO");
         return product;
    }

    private Product getProductBaseMalt() {
        Product product = new Product();
        product.setId(1);
        product.setProductCode("1600C");
        product.setProductDescription("2-Row");
        product.setProductPrice("50");
        product.setTotalWeight("55");
        product.setBaseMalt("YES");
        return product;
    }

    private Split getSplit() {
        Split split = new Split();
        split.setId(1);
        split.setProductCode("1600C");
        split.setProductDescription("2-Row");
        split.setProductPrice("50");
        split.setTotalWeight("50");
        split.setAvailable("30");
        split.setLocked("NO");
        split.setStartedBy("user");
        split.setPricePerPound("1");
        split.setClaimed("20");
        split.setStatus("OPEN");
        split.setPaid("NO");
        return split;
    }

    private UserSplit getUserSplit() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setProductCode("1600C");
        userSplit.setProductDescription("2-Row");
        userSplit.setProductPrice("50");
        userSplit.setTotalWeight("50");
        userSplit.setStartedBy("user");
        userSplit.setPricePerPound("1");
        userSplit.setClaimed("20");
        userSplit.setStatus("CLOSED");
        userSplit.setPaid("NO");
        userSplit.setSplitId("1");
        userSplit.setUsername("user");
        userSplit.setSplitTotal("20");
        return userSplit;
    }
}