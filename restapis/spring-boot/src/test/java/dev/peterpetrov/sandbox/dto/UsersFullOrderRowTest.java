package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsersFullOrderRowTest {

    @Test
    void getId() {
        UsersFullOrderRow usersFullOrderRow = new UsersFullOrderRow();
        usersFullOrderRow.setId(1);
        assertThat(usersFullOrderRow.getId()).isEqualTo(1);
    }

    @Test
    void allArgsConstructor() {
        UsersFullOrderRow usersFullOrderRow = new UsersFullOrderRow(1, "2", "Full Sack", "user", "1001", "2-Row", "50", "Open", "1", "10", "10");
        assertThat(usersFullOrderRow.getId()).isEqualTo(1);
        assertThat(usersFullOrderRow.getSackId()).isEqualTo("2");
        assertThat(usersFullOrderRow.getType()).isEqualTo("Full Sack");
        assertThat(usersFullOrderRow.getUsername()).isEqualTo("user");
        assertThat(usersFullOrderRow.getProductCode()).isEqualTo("1001");
        assertThat(usersFullOrderRow.getProductDescription()).isEqualTo("2-Row");
        assertThat(usersFullOrderRow.getProductPrice()).isEqualTo("50");
        assertThat(usersFullOrderRow.getStatus()).isEqualTo("Open");
        assertThat(usersFullOrderRow.getPricePerPound()).isEqualTo("1");
        assertThat(usersFullOrderRow.getClaimed()).isEqualTo("10");
        assertThat(usersFullOrderRow.getTotal()).isEqualTo("10");
    }
}