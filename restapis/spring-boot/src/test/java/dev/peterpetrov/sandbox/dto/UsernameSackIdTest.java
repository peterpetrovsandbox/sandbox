package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsernameSackIdTest {

    @Test
    void getUsername() {
        UsernameSackId usernameSackId = new UsernameSackId();
        usernameSackId.setSackId(1);
        assertThat(usernameSackId.getSackId()).isEqualTo(1);
    }

    @Test
    void allArgsConstructor() {
        UsernameSackId usernameSackId = new UsernameSackId("user", 1);
        assertThat(usernameSackId.getUsername()).isEqualTo("user");
        assertThat(usernameSackId.getSackId()).isEqualTo(1);
    }
}