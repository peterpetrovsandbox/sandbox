package dev.peterpetrov.sandbox.rest;

import dev.peterpetrov.sandbox.entity.Product;
import dev.peterpetrov.sandbox.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProductRestControllerTest {

    private ProductRepository productRepository;
    private ProductRestController productRestController;

    @BeforeEach
    void setUp() {
        productRepository = mock(ProductRepository.class);
        productRestController = new ProductRestController(productRepository);
    }

    @Test
    void getProduct() {
        when(productRepository.findAll()).thenReturn(getProducts());
        ResponseEntity<List<Product>> responseEntity = productRestController.getProduct();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getProducts());
    }

    private List<Product> getProducts() {
        Product product = new Product();
        product.setId(1);
        product.setProductCode("1001");
        product.setProductDescription("2-Row");
        product.setProductPrice("50");
        product.setTotalWeight("50");
        product.setBaseMalt("YES");
        return new ArrayList<>(List.of(product));
    }
}