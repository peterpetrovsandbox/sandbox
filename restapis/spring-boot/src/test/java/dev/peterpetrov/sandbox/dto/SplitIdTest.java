package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SplitIdTest {

    @Test
    void getId() {
        SplitId splitId = new SplitId();
        splitId.setId("1");
        assertThat(splitId.getId()).isEqualTo("1");
    }

    @Test
    void allArgsConstructor() {
        SplitId splitId = new SplitId("1");
        assertThat(splitId.getId()).isEqualTo("1");
    }
}