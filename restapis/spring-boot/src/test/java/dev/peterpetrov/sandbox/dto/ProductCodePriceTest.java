package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ProductCodePriceTest {

    @Test
    void getProductCode() {
        ProductCodePrice productCodePrice = new ProductCodePrice();
        productCodePrice.setProductCode("1001");
        assertThat(productCodePrice.getProductCode()).isEqualTo("1001");
    }

    @Test
    void allArgsConstructor() {
        ProductCodePrice productCodePrice = new ProductCodePrice("1001", "50");
        assertThat(productCodePrice.getProductCode()).isEqualTo("1001");
        assertThat(productCodePrice.getPrice()).isEqualTo("50");
    }
}