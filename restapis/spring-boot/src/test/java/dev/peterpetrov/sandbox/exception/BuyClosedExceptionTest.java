package dev.peterpetrov.sandbox.exception;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class BuyClosedExceptionTest {

    @Test
    void testException() {
        String message = "Buy closed";
        BuyClosedException buyClosedException = new BuyClosedException(message);
        assertThat(buyClosedException.getMessage()).isEqualTo(message);
    }
}