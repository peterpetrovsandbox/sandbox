package dev.peterpetrov.sandbox.rest;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.Split;
import dev.peterpetrov.sandbox.entity.UserSplit;
import dev.peterpetrov.sandbox.service.SplitsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SplitsRestControllerTest {

    private SplitsService splitsService;
    private SplitsRestController splitsRestController;

    @BeforeEach
    void setUp() {
        splitsService = mock(SplitsService.class);
        splitsRestController = new SplitsRestController(splitsService);
    }

    @Test
    void startSplit() {
        when(splitsService.startSplit(any(UsernameProductCode.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = splitsRestController.startSplit(new UsernameProductCode());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void joinSplit() {
        when(splitsService.joinSplit(any(UsernameSplitIdPounds.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = splitsRestController.joinSplit(new UsernameSplitIdPounds());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void removeSplitUser() {
        when(splitsService.removeSplitUser(any(UsernameSplitId.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = splitsRestController.removeSplitUser(new UsernameSplitId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void removeSplitAdmin() {
        when(splitsService.removeUserSplitAdmin(any(SplitId.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = splitsRestController.removeSplitAdmin(new SplitId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void removeFullSplit() {
        when(splitsService.removeFullSplit(any(TableId.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = splitsRestController.removeFullSplit(new TableId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void removeAllOpenSplits() {
        when(splitsService.removeAllOpenSplits()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = splitsRestController.removeAllOpenSplits();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void myOrderSplits() {
        when(splitsService.myOrderSplits(any(Username.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getMyUserSplits()));
        ResponseEntity<List<UserSplit>> responseEntity = splitsRestController.myOrderSplits(new Username());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getMyUserSplits());
    }

    @Test
    void getSplits() {
        when(splitsService.getSplits()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(splits()));
        ResponseEntity<List<Split>> responseEntity = splitsRestController.getSplits();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(splits());
    }

    @Test
    void getUserSplits() {
        when(splitsService.getUsersSplits()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(userSplits()));
        ResponseEntity<List<UserSplit>> responseEntity = splitsRestController.getUserSplits();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(userSplits());
    }

    private List<UserSplit> getMyUserSplits() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setSplitId("2");
        userSplit.setSplitTotal("10");
        userSplit.setUsername("user");
        userSplit.setStatus("Open");
        userSplit.setClaimed("10");
        userSplit.setPricePerPound("1");
        userSplit.setProductCode("1001");
        userSplit.setProductDescription("2-Row");
        userSplit.setStartedBy("user");
        userSplit.setTotalWeight("50");
        userSplit.setPaid("YES");
        userSplit.setProductPrice("50");
        return new ArrayList<>(List.of(userSplit));
    }

    private List<Split> splits() {
        Split split = new Split();
        split.setId(1);
        split.setAvailable("30");
        split.setStatus("Open");
        split.setClaimed("10");
        split.setLocked("NO");
        split.setPricePerPound("1");
        split.setProductCode("1001");
        split.setProductDescription("2-Row");
        split.setStartedBy("user");
        split.setProductPrice("50");
        split.setTotalWeight("50");
        split.setPaid("NO");
        return new ArrayList<>(List.of(split));
    }

    private List<UserSplit> userSplits() {
        UserSplit userSplit = new UserSplit();
        userSplit.setId(1);
        userSplit.setStatus("OPEN");
        userSplit.setSplitId("2");
        userSplit.setProductPrice("50");
        userSplit.setTotalWeight("50");
        userSplit.setStartedBy("user");
        userSplit.setProductDescription("2-Row");
        userSplit.setProductCode("1001");
        userSplit.setPricePerPound("1");
        userSplit.setClaimed("10");
        userSplit.setPaid("YES");
        userSplit.setSplitTotal("10");
        userSplit.setUsername("user");
        return new ArrayList<>(List.of(userSplit));
    }
}