package dev.peterpetrov.sandbox.service;

import dev.peterpetrov.sandbox.entity.Random;
import dev.peterpetrov.sandbox.repository.RandomRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class UtilitiesServiceTest {

    private RandomRepository randomRepository;
    private UtilitiesService utilitiesService;

    @BeforeEach
    void setUp() {
        randomRepository = mock(RandomRepository.class);
        utilitiesService = new UtilitiesService(randomRepository);
    }

    @Test
    void checkIfBuyIsOpen() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomOpen());
        utilitiesService.checkIfBuyIsOpen();
        verify(randomRepository, times(1)).findById(anyInt());
    }

    @Test
    void checkIfBuyIsClosed() {
        when(randomRepository.findById(anyInt())).thenReturn(getRandomClosed());
        assertThatThrownBy(() -> utilitiesService.checkIfBuyIsOpen())
                .hasNoCause()
                .hasMessage("Buy is currently closed");
    }

    private Random getRandomOpen() {
        Random random = new Random();
        random.setUserAction("Open");
        return random;
    }

    private Random getRandomClosed() {
        Random random = new Random();
        random.setUserAction("Closed");
        return random;
    }
}