package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FullSackTest {

    @Test
    void allFields() {
        FullSack fullSack = new FullSack();
        fullSack.setId(1);
        fullSack.setProductCode("1001");
        fullSack.setProductDescription("2-Row");
        fullSack.setProductPrice("50");
        fullSack.setTotalWeight("55");
        fullSack.setUsername("user");
        fullSack.setPaid("YES");
        assertThat(fullSack.getId()).isEqualTo(1);
        assertThat(fullSack.getProductCode()).isEqualTo("1001");
        assertThat(fullSack.getProductDescription()).isEqualTo("2-Row");
        assertThat(fullSack.getProductPrice()).isEqualTo("50");
        assertThat(fullSack.getTotalWeight()).isEqualTo("55");
        assertThat(fullSack.getUsername()).isEqualTo("user");
        assertThat(fullSack.getPaid()).isEqualTo("YES");
    }
}