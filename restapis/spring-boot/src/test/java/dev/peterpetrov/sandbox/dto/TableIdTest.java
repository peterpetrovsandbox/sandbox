package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TableIdTest {

    @Test
    void getId() {
        TableId tableId = new TableId();
        tableId.setId("1");
        assertThat(tableId.getId()).isEqualTo("1");
    }

    @Test
    void allArgsConstructor() {
        TableId tableId = new TableId("1");
        assertThat(tableId.getId()).isEqualTo("1");
    }
}