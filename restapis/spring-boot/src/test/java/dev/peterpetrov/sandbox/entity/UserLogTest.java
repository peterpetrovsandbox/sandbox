package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UserLogTest {

    @Test
    void allFields() {
        UserLog userLog = new UserLog();
        userLog.setId(1);
        userLog.setTimestamp("17:03:11 EST");
        userLog.setUsername("user");
        userLog.setUserAction("insert");
        assertThat(userLog.getId()).isEqualTo(1);
        assertThat(userLog.getTimestamp()).isEqualTo("17:03:11 EST");
        assertThat(userLog.getUsername()).isEqualTo("user");
        assertThat(userLog.getUserAction()).isEqualTo("insert");
    }
}