package dev.peterpetrov.sandbox.dto;


import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BuyerOrderByProductCodeRowTest {

    @Test
    void getProductCode() {
        BuyerOrderByProductCodeRow buyerOrderByProductCodeRow = new BuyerOrderByProductCodeRow();
        buyerOrderByProductCodeRow.setProductCode("1001");
        assertThat(buyerOrderByProductCodeRow.getProductCode()).isEqualTo("1001");
    }

    @Test
    void allArgsConstructor() {
        BuyerOrderByProductCodeRow buyerOrderByProductCodeRow = new BuyerOrderByProductCodeRow("1001", "2-Row", 1.00, 5, 5.00);
        assertThat(buyerOrderByProductCodeRow.getProductCode()).isEqualTo("1001");
        assertThat(buyerOrderByProductCodeRow.getProductDescription()).isEqualTo("2-Row");
        assertThat(buyerOrderByProductCodeRow.getProductPrice()).isEqualTo(1.00);
        assertThat(buyerOrderByProductCodeRow.getQuantity()).isEqualTo(5);
        assertThat(buyerOrderByProductCodeRow.getTotal()).isEqualTo(5.00);
    }
}