package dev.peterpetrov.sandbox.rest;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.FullSack;
import dev.peterpetrov.sandbox.service.FullSacksService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FullSackRestControllerTest {

    private static final String SUCCESS = "Success";

    private FullSacksRestController fullSacksRestController;
    private FullSacksService fullSacksService;

    @BeforeEach
    void setUp() {
        fullSacksService = mock(FullSacksService.class);
        fullSacksRestController = new FullSacksRestController(fullSacksService);
    }

    @Test
    void insertFullSack() {
        when(fullSacksService.insertFullSack(any(UsernameProductCode.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success(SUCCESS)));
        ResponseEntity<Success> responseEntity = fullSacksRestController.insertFullSack(new UsernameProductCode("user", "1001"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success(SUCCESS));
    }

    @Test
    void removeFullSack() {
        when(fullSacksService.removeFullSack(any(UsernameSackId.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success(SUCCESS)));
        ResponseEntity<Success> responseEntity = fullSacksRestController.removeFullSack(new UsernameSackId("user", 1));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success(SUCCESS));
    }

    @Test
    void adminRemoveFullSack() {
        when(fullSacksService.adminRemoveSack(any(TableId.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success(SUCCESS)));
        ResponseEntity<Success> responseEntity = fullSacksRestController.adminRemoveFullSack(new TableId("1"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success(SUCCESS));
    }

    @Test
    void myOrderFullSacks() {
        when(fullSacksService.myOrderFullSacks(any(Username.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getFullSacks()));
        ResponseEntity<List<FullSack>> responseEntity = fullSacksRestController.myOrderFullSacks(new Username("user"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getFullSacks());
    }

    @Test
    void fullSacks() {
        when(fullSacksService.getFullSacks()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getFullSacks()));
        ResponseEntity<List<FullSack>> responseEntity = fullSacksRestController.fullSacks();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getFullSacks());
    }

    private List<FullSack> getFullSacks() {
        FullSack fullSack = new FullSack();
        fullSack.setId(1);
        fullSack.setProductCode("1001");
        fullSack.setProductDescription("2-Row");
        fullSack.setProductPrice("50");
        fullSack.setTotalWeight("50");
        fullSack.setUsername("user");
        fullSack.setPaid("YES");
        return new ArrayList<>(List.of(fullSack));
    }
}