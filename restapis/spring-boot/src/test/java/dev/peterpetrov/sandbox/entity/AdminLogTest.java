package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AdminLogTest {

    @Test
    void allFields() {
        AdminLog adminLog = new AdminLog();
        adminLog.setId(1);
        adminLog.setTimestamp("17:03:11 EST");
        adminLog.setException("IOException");
        assertThat(adminLog.getId()).isEqualTo(1);
        assertThat(adminLog.getTimestamp()).isEqualTo("17:03:11 EST");
        assertThat(adminLog.getException()).isEqualTo("IOException");
    }
}