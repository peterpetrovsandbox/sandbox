package dev.peterpetrov.sandbox.rest;

import dev.peterpetrov.sandbox.dto.Message;
import dev.peterpetrov.sandbox.dto.ProductCodePrice;
import dev.peterpetrov.sandbox.dto.Success;
import dev.peterpetrov.sandbox.dto.Username;
import dev.peterpetrov.sandbox.entity.UserLog;
import dev.peterpetrov.sandbox.service.RandomService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RandomRestControllerTest {

    private RandomService randomService;
    private RandomRestController randomRestController;

    @BeforeEach
    void setUp() {
        randomService = mock(RandomService.class);
        randomRestController = new RandomRestController(randomService);
    }

    @Test
    void openCloseBuy() {
        when(randomService.openCloseBuy()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = randomRestController.openCloseBuy();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void updateMessage() {
        when(randomService.updateMessage(any(Message.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = randomRestController.updateMessage(new Message("hello world"));
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    @Test
    void currentOrderCount() {
        when(randomService.currentOrderCount()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Message("5")));
        ResponseEntity<Message> responseEntity = randomRestController.currentOrderCount();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("5"));
    }

    @Test
    void getUserLog() {
        when(randomService.getUserLog()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(getUserLogTest()));
        ResponseEntity<List<UserLog>> responseEntity = randomRestController.getUserLog();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(getUserLogTest());
    }

    @Test
    void getBuyStatus() {
        when(randomService.getBuyStatus()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Message("Open")));
        ResponseEntity<Message> responseEntity = randomRestController.getBuyStatus();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("Open"));
    }

    @Test
    void getMessage() {
        when(randomService.getMessage()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Message("message")));
        ResponseEntity<Message> responseEntity = randomRestController.getMessage();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("message"));
    }

    @Test
    void getUserTotal() {
        when(randomService.getUserTotal(anyString())).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Message("10")));
        ResponseEntity<Message> responseEntity = randomRestController.getUserTotal("user");
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("10"));
    }

    @Test
    void orderTotal() {
        when(randomService.orderTotal()).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Message("10")));
        ResponseEntity<Message> responseEntity = randomRestController.orderTotal();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Message("10"));
    }

    @Test
    void setPrice() {
        when(randomService.setPrice(any(ProductCodePrice.class))).thenReturn(ResponseEntity.status(HttpStatus.OK).body(new Success("Success")));
        ResponseEntity<Success> responseEntity = randomRestController.setPrice(new ProductCodePrice());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isNotNull();
        assertThat(responseEntity.getBody()).usingRecursiveComparison().isEqualTo(new Success("Success"));
    }

    private List<UserLog> getUserLogTest() {
        UserLog userLog = new UserLog();
        userLog.setId(1);
        userLog.setUsername("user");
        userLog.setUserAction("insert");
        userLog.setTimestamp("17:05:22 EST");
        return new ArrayList<>(List.of(userLog));
    }
}