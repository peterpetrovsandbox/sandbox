package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsernameProductCodeTest {

    @Test
    void getUsername() {
        UsernameProductCode usernameProductCode = new UsernameProductCode();
        usernameProductCode.setUsername("user");
        assertThat(usernameProductCode.getUsername()).isEqualTo("user");
    }

    @Test
    void allArgsConstructor() {
        UsernameProductCode usernameProductCode = new UsernameProductCode("user", "1001");
        assertThat(usernameProductCode.getUsername()).isEqualTo("user");
        assertThat(usernameProductCode.getProductCode()).isEqualTo("1001");
    }
}