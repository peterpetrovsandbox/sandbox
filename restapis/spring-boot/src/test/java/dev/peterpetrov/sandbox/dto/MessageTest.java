package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MessageTest {

    @Test
    void getAnswer() {
        Message message = new Message();
        message.setPayload("hello world");
        assertThat(message.getPayload()).isEqualTo("hello world");
    }

    @Test
    void allArgsConstructor() {
        Message message = new Message("hello world");
        assertThat(message.getPayload()).isEqualTo("hello world");
    }
}