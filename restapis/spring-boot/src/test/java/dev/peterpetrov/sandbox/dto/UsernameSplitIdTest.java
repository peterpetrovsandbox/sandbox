package dev.peterpetrov.sandbox.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UsernameSplitIdTest {

    @Test
    void getSplitId() {
        UsernameSplitId usernameSplitId = new UsernameSplitId();
        usernameSplitId.setSplitId("2");
        assertThat(usernameSplitId.getSplitId()).isEqualTo("2");
    }

    @Test
    void allArgsConstructor() {
        UsernameSplitId usernameSplitId = new UsernameSplitId("user", "2");
        assertThat(usernameSplitId.getUsername()).isEqualTo("user");
        assertThat(usernameSplitId.getSplitId()).isEqualTo("2");
    }
}