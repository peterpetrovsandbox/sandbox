package dev.peterpetrov.sandbox.entity;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SplitTest {

    @Test
    void allFields() {
        Split split = new Split();
        split.setId(1);
        split.setProductCode("1001");
        split.setProductDescription("2-Row");
        split.setProductPrice("50");
        split.setTotalWeight("55");
        split.setPaid("YES");
        split.setStartedBy("user");
        split.setPricePerPound("1.50");
        split.setClaimed("10");
        split.setStatus("OPEN");
        split.setAvailable("45");
        split.setLocked("YES");
        assertThat(split.getId()).isEqualTo(1);
        assertThat(split.getProductCode()).isEqualTo("1001");
        assertThat(split.getProductDescription()).isEqualTo("2-Row");
        assertThat(split.getProductPrice()).isEqualTo("50");
        assertThat(split.getTotalWeight()).isEqualTo("55");
        assertThat(split.getPaid()).isEqualTo("YES");
        assertThat(split.getStartedBy()).isEqualTo("user");
        assertThat(split.getPricePerPound()).isEqualTo("1.50");
        assertThat(split.getClaimed()).isEqualTo("10");
        assertThat(split.getStatus()).isEqualTo("OPEN");
        assertThat(split.getAvailable()).isEqualTo("45");
        assertThat(split.getLocked()).isEqualTo("YES");
    }
}