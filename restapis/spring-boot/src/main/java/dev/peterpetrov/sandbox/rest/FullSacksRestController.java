package dev.peterpetrov.sandbox.rest;

import java.util.List;

import dev.peterpetrov.sandbox.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import dev.peterpetrov.sandbox.entity.FullSack;
import dev.peterpetrov.sandbox.service.FullSacksService;

@RestController
@CrossOrigin
@RequestMapping("full-sack")
public class FullSacksRestController {

    private final FullSacksService fullSacksService;

    @Autowired
    public FullSacksRestController(FullSacksService fullSacksService) {
        this.fullSacksService = fullSacksService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> insertFullSack(@RequestBody UsernameProductCode usernameProductCode) {
        return fullSacksService.insertFullSack(usernameProductCode);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> removeFullSack(@RequestBody UsernameSackId usernameSackId) {
        return fullSacksService.removeFullSack(usernameSackId);
    }

    @DeleteMapping(path = "/admin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> adminRemoveFullSack(@RequestBody TableId tableId) {
        return fullSacksService.adminRemoveSack(tableId);
    }

    @GetMapping(path = "/my-order", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FullSack>> myOrderFullSacks(@RequestBody Username username) {
        return fullSacksService.myOrderFullSacks(username);
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FullSack>> fullSacks() {
        return fullSacksService.getFullSacks();
    }
}
