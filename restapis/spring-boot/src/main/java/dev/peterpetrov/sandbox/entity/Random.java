package dev.peterpetrov.sandbox.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "random")
public class Random {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "user_action")
    private String userAction;
}
