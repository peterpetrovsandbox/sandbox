package dev.peterpetrov.sandbox.rest;

import java.util.List;

import dev.peterpetrov.sandbox.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import dev.peterpetrov.sandbox.entity.Split;
import dev.peterpetrov.sandbox.entity.UserSplit;
import dev.peterpetrov.sandbox.service.SplitsService;

@RestController
@CrossOrigin
@RequestMapping("splits")
public class SplitsRestController {

    private final SplitsService splitsService;

    @Autowired
    public SplitsRestController(SplitsService splitsService) {
        this.splitsService = splitsService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> startSplit(@RequestBody UsernameProductCode usernameProductCode) {
        return splitsService.startSplit(usernameProductCode);
    }

    @PostMapping(path = "/join", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> joinSplit(@RequestBody UsernameSplitIdPounds usernameSplitIdPounds) {
        return splitsService.joinSplit(usernameSplitIdPounds);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> removeSplitUser(@RequestBody UsernameSplitId usernameSplitId) {
        return splitsService.removeSplitUser(usernameSplitId);
    }

    @DeleteMapping(path = "/user-split-admin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> removeSplitAdmin(@RequestBody SplitId splitId) {
        return splitsService.removeUserSplitAdmin(splitId);
    }

    @DeleteMapping(path = "/full-split-admin", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> removeFullSplit(@RequestBody TableId tableId) {
        return splitsService.removeFullSplit(tableId);
    }

    @DeleteMapping(path = "/all-open", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> removeAllOpenSplits() {
        return splitsService.removeAllOpenSplits();
    }

    @GetMapping(path = "/my-order", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserSplit>> myOrderSplits(@RequestBody Username username) {
        return splitsService.myOrderSplits(username);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Split>> getSplits() {
        return splitsService.getSplits();
    }

    @GetMapping(path = "/user-splits", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserSplit>> getUserSplits() {
        return splitsService.getUsersSplits();
    }
}
