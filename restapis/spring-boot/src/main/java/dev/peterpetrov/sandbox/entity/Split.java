package dev.peterpetrov.sandbox.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "splits")
public class Split {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "product_price")
    private String productPrice;

    @Column(name = "total_weight")
    private String totalWeight;

    @Column(name = "available")
    private String available;

    @Column(name = "locked")
    private String locked;

    @Column(name = "started_by")
    private String startedBy;

    @Column(name = "price_per_pound")
    private String pricePerPound;

    @Column(name = "claimed")
    private String claimed;

    @Column(name = "status")
    private String status;

    @Column(name = "paid")
    private String paid;
}
