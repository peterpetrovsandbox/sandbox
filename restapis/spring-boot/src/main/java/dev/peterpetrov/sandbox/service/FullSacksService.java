package dev.peterpetrov.sandbox.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.Product;
import dev.peterpetrov.sandbox.entity.UserLog;
import dev.peterpetrov.sandbox.exception.FullSacksException;
import dev.peterpetrov.sandbox.repository.UserLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import dev.peterpetrov.sandbox.entity.FullSack;
import dev.peterpetrov.sandbox.repository.FullSacksRepository;
import dev.peterpetrov.sandbox.repository.ProductRepository;

@Service
public class FullSacksService {
    private final FullSacksRepository fullSacksRepository;
    private final ProductRepository productRepository;
    private final UserLogRepository userLogRepository;
    private final UtilitiesService utilitiesService;

    @Autowired
    public FullSacksService(FullSacksRepository fullSacksRepository, ProductRepository productRepository, UserLogRepository userLogRepository, UtilitiesService utilitiesService) {
        this.fullSacksRepository = fullSacksRepository;
        this.productRepository = productRepository;
        this.userLogRepository = userLogRepository;
        this.utilitiesService = utilitiesService;
    }

    public ResponseEntity<Success> insertFullSack(UsernameProductCode usernameProductCode) {
        utilitiesService.checkIfBuyIsOpen();
        Product product = productRepository.findByProductCode(usernameProductCode.getProductCode());
        if (product == null) {
            throw new FullSacksException("No such product code");
        }
        fullSacksRepository.save(buildFullSackFromRequest(usernameProductCode, product));
        userLogRepository.save(buildUserLogFromRequest(String.format("Added %s to their order", product.getProductDescription()), usernameProductCode.getUsername()));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Order full sack successful"));
    }

    public ResponseEntity<Success> removeFullSack(UsernameSackId usernameSackId) {
        utilitiesService.checkIfBuyIsOpen();
        FullSack fullSack = fullSacksRepository.findFullSacksById(usernameSackId.getSackId());
        if (fullSack == null) {
            throw new FullSacksException("No such sack id");
        }
        fullSacksRepository.deleteByIdAndUsername(usernameSackId.getSackId(), usernameSackId.getUsername());
        userLogRepository.save(buildUserLogFromRequest(String.format("Removed %s from their order", fullSack.getProductDescription()), usernameSackId.getUsername()));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Remove full sack successful"));
    }

    public ResponseEntity<Success> adminRemoveSack(TableId tableId) {
        FullSack fullSack = fullSacksRepository.findFullSacksById(Integer.parseInt(tableId.getId()));
        if (fullSack == null) {
            throw new FullSacksException("No such table id");
        }
        fullSacksRepository.delete(fullSack);
        userLogRepository.save(buildUserLogFromRequest(String.format("Removed sack %s %s from the order", fullSack.getId(), fullSack.getProductDescription()), "Admin"));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Sack removed successful"));
    }

    public ResponseEntity<List<FullSack>> myOrderFullSacks(Username username) {
        List<FullSack> fullSacks = fullSacksRepository.findAllByUsername(username.getName());
        if (fullSacks.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }
        return ResponseEntity.status(HttpStatus.OK).body(fullSacks);
    }

    public ResponseEntity<List<FullSack>> getFullSacks() {
        List<FullSack> fullSacksList = fullSacksRepository.findAll();
        if (fullSacksList.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<>());
        }
        return ResponseEntity.status(HttpStatus.OK).body(fullSacksList);
    }

    private FullSack buildFullSackFromRequest(UsernameProductCode usernameProductCode, Product product) {
        FullSack fullSack = new FullSack();
        fullSack.setUsername(usernameProductCode.getUsername());
        fullSack.setProductCode(product.getProductCode());
        fullSack.setProductPrice(usernameProductCode.getProductCode());
        fullSack.setProductDescription(product.getProductDescription());
        fullSack.setProductPrice(product.getProductPrice());
        fullSack.setTotalWeight(product.getTotalWeight());
        fullSack.setPaid("NO");
        return fullSack;
    }

    private UserLog buildUserLogFromRequest(String action, String username) {
        UserLog userLog = new UserLog();
        userLog.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        userLog.setUserAction(action);
        userLog.setUsername(username);
        return userLog;
    }
}
