package dev.peterpetrov.sandbox.service;

import dev.peterpetrov.sandbox.entity.Random;
import dev.peterpetrov.sandbox.exception.BuyClosedException;
import dev.peterpetrov.sandbox.repository.RandomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilitiesService {

    private final RandomRepository randomRepository;

    @Autowired
    public UtilitiesService(RandomRepository randomRepository) {
        this.randomRepository = randomRepository;
    }

    public void checkIfBuyIsOpen() {
        Random random = randomRepository.findById(1);
        if (!random.getUserAction().equals("Open")) throw new BuyClosedException("Buy is currently closed");
    }
}
