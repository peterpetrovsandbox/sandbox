package dev.peterpetrov.sandbox.service;

import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import dev.peterpetrov.sandbox.dto.*;
import dev.peterpetrov.sandbox.entity.UserLog;
import dev.peterpetrov.sandbox.exception.SplitsException;
import dev.peterpetrov.sandbox.exception.FullSacksException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import dev.peterpetrov.sandbox.entity.Product;
import dev.peterpetrov.sandbox.entity.Split;
import dev.peterpetrov.sandbox.entity.UserSplit;
import dev.peterpetrov.sandbox.repository.ProductRepository;
import dev.peterpetrov.sandbox.repository.SplitsRepository;
import dev.peterpetrov.sandbox.repository.UserLogRepository;
import dev.peterpetrov.sandbox.repository.UserSplitsRepository;

@Service
public class SplitsService {

    private final SplitsRepository splitsRepository;
    private final ProductRepository productRepository;
    private final UserLogRepository userLogRepository;
    private final UserSplitsRepository userSplitsRepository;
    private final UtilitiesService utilitiesService;

    private static final String CLOSED = "CLOSED";
    private static final String OPEN = "OPEN";
    private static final String YES = "YES";
    private static final String NO = "NO";
    private static final String ADMIN = "Admin";

    @Autowired
    public SplitsService(SplitsRepository splitsRepository, ProductRepository productRepository,
                         UserLogRepository userLogRepository, UserSplitsRepository userSplitsRepository, UtilitiesService utilitiesService) {
        this.splitsRepository = splitsRepository;
        this.productRepository = productRepository;
        this.userLogRepository = userLogRepository;
        this.userSplitsRepository = userSplitsRepository;
        this.utilitiesService = utilitiesService;
    }

    public ResponseEntity<Success> startSplit(UsernameProductCode usernameProductCode) {
        utilitiesService.checkIfBuyIsOpen();
        Product product = getProductFromDatabase(usernameProductCode);
        checkIfProductIsBaseMalt(product);
        checkIfCurrentOpenSplitExists(usernameProductCode.getProductCode());
        return handleSplitStart(usernameProductCode.getUsername(), product);
    }

    public ResponseEntity<Success> joinSplit(UsernameSplitIdPounds usernameSplitIdPounds) {
        utilitiesService.checkIfBuyIsOpen();
        String username = usernameSplitIdPounds.getUsername();
        String splitId = usernameSplitIdPounds.getSplitId();
        String pounds = usernameSplitIdPounds.getPounds();
        Split split = fetchOpenSplit(splitId);
        checkAvailable(usernameSplitIdPounds.getPounds(), split.getAvailable());
        checkIfFivePoundIncrement(usernameSplitIdPounds.getPounds());
        return hasNotAlreadyJoinedSplit(splitId, username) ? joinSplit(split, pounds, username, splitId) : addToSplit(split, pounds, splitId, username);
    }

    public ResponseEntity<Success> removeSplitUser(UsernameSplitId usernameSplitId) {
        utilitiesService.checkIfBuyIsOpen();
        return removeSplitUser(usernameSplitId.getSplitId(), usernameSplitId.getUsername());
    }

    public ResponseEntity<Success> removeUserSplitAdmin(SplitId splitId) {
        UserSplit userSplit = userSplitsRepository.findById(Integer.parseInt(splitId.getId()));
        if (userSplit == null) {
            throw new SplitsException("No such table id");
        }
        return removeUserSplit(userSplit, ADMIN);
    }

    public ResponseEntity<Success> removeAllOpenSplits() {
        List<Split> openSplits = splitsRepository.findByStatus(OPEN);
        List<UserSplit> openUserSplits = userSplitsRepository.findByStatus(OPEN);
        if (openSplits != null) {
            splitsRepository.deleteInBatch(openSplits);
        }
        if (openUserSplits != null) {
            userSplitsRepository.deleteInBatch(openUserSplits);
        }
        userLogRepository.save(buildUserLogSplit("Removed all open splits", ADMIN));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Open splits removal successful"));
    }

    public ResponseEntity<Success> removeFullSplit(TableId tableId) {
        splitsRepository.deleteById(Integer.parseInt(tableId.getId()));
        userSplitsRepository.deleteAllBySplitId(tableId.getId());
        userLogRepository.save(buildUserLogSplit(String.format("Removed split %s and all corresponding user splits", tableId.getId()), ADMIN));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Split and corresponding user splits removed successfully"));
    }

    public ResponseEntity<List<UserSplit>> myOrderSplits(Username username) {
        List<UserSplit> userSplits = userSplitsRepository.findAllByUsername(username.getName());
        return ResponseEntity.status(HttpStatus.OK).body(Objects.requireNonNullElseGet(userSplits, ArrayList::new));
    }

    public ResponseEntity<List<Split>> getSplits() {
        List<Split> splitsList = splitsRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(splitsList);
    }

    public ResponseEntity<List<UserSplit>> getUsersSplits() {
        List<UserSplit> userSplitsList = userSplitsRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(userSplitsList);
    }

    private String calculatePricePerPound(String productPrice, String productWeight) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        double productPriceDouble = Double.parseDouble(productPrice);
        double productWeightDouble = Double.parseDouble(productWeight);
        return decimalFormat.format(productPriceDouble / productWeightDouble);
    }

    private ResponseEntity<Success> handleSplitStart(String username, Product product) {
        splitsRepository.save(buildSplitFromRequest(username, product));
        userLogRepository.save(buildUserLogNewSplit(username, product));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Split started successful"));
    }

    private void checkIfCurrentOpenSplitExists(String productCode) {
        if (splitsRepository.findSplitsByProductCodeAndStatus(productCode, OPEN) != null) {
            throw new SplitsException("There is an existing open split");
        }
    }

    private boolean splitIsBaseMalt(Product product) {
        return product.getBaseMalt().equals(YES);
    }

    private void checkAvailable(String claimed, String available) {
        if (Integer.parseInt(claimed) > Integer.parseInt(available)) {
            throw new SplitsException("Claiming more than available");
        }
    }

    private void checkIfFivePoundIncrement(String pounds) {
        if (Integer.parseInt(pounds) == 0 || Integer.parseInt(pounds) % 5 != 0) {
            throw new SplitsException("Must order in 5 Lb increments");
        }
    }

    private boolean hasNotAlreadyJoinedSplit(String splitId, String username) {
        return userSplitsRepository.findBySplitIdAndUsername(splitId, username) == null;
    }

    private String calculateSplitTotal(String pricePerPound, String claimed) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        Double pricePerPoundDouble = Double.parseDouble(pricePerPound);
        Double claimedDouble = Double.parseDouble(claimed);
        return decimalFormat.format(pricePerPoundDouble * claimedDouble);
    }

    private String calculateClaimed(String current, String additional) {
        return String.valueOf(Integer.parseInt(current) + Integer.parseInt(additional));
    }

    private String calculateAvailable(String current, String claimed) {
        return String.valueOf(Integer.parseInt(current) - Integer.parseInt(claimed));
    }

    private String setNewStatus(String available) {
        return available.equals("0") ? CLOSED : OPEN;
    }

    private void changeStatusToAllUserSplits(int splitId, String action) {
        List<UserSplit> allSackSplits = userSplitsRepository.findAllBySplitId(String.valueOf(splitId));
        for (UserSplit userSplit : allSackSplits) {
            userSplit.setStatus(action);
            userSplitsRepository.save(userSplit);
        }
    }

    private ResponseEntity<Success> removeUserSplit(UserSplit userSplit, String username) {
        Split split = splitsRepository.findById(Integer.parseInt(userSplit.getSplitId()));
        String userTakenPounds = userSplit.getClaimed();
        if (userSplit.getStatus().equals(CLOSED)) {
            changeStatusToAllUserSplits(Integer.parseInt(userSplit.getSplitId()), OPEN);
        }
        userSplitsRepository.delete(userSplit);
        String splitsClaimed = split.getClaimed();
        String splitsAvailable = split.getAvailable();
        split.setClaimed(String.valueOf(Integer.parseInt(splitsClaimed) - Integer.parseInt(userTakenPounds)));
        split.setAvailable(String.valueOf(Integer.parseInt(splitsAvailable) + Integer.parseInt(userTakenPounds)));
        split.setStatus(OPEN);
        splitsRepository.save(split);
        userLogRepository.save(buildUserLogSplit(String.format("Removed %s lbs from split %s %s", userTakenPounds, split.getId(), split.getProductDescription()), username));
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Split remove successful"));
    }

    private Product getProductFromDatabase(UsernameProductCode usernameProductCode) {
        Product product = productRepository.findByProductCode(usernameProductCode.getProductCode());
        if (product == null) {
            throw new FullSacksException("No such product code");
        }
        return product;
    }

    private void checkIfProductIsBaseMalt(Product product) {
        if (splitIsBaseMalt(product)) {
            throw new SplitsException("Base malt, cannot start split");
        }
    }

    private Split fetchOpenSplit(String splitId) {
        Split openSplit = splitsRepository.findById(Integer.parseInt(splitId));
        if (openSplit == null) {
            throw new SplitsException("No such split id");
        }
        return openSplit;
    }

    private ResponseEntity<Success> joinSplit(Split split, String pounds, String username, String splitId) {
        String available = calculateAvailable(split.getAvailable(), pounds);
        String openOrClosedSplit = setNewStatus(available);
        userSplitsRepository.save(buildSplitFromRequest(split, pounds, username, openOrClosedSplit));
        split.setClaimed(calculateClaimed(split.getClaimed(), pounds));
        split.setAvailable(available);
        split.setStatus(openOrClosedSplit);
        splitsRepository.save(split);
        userLogRepository.save(buildUserLogSplit(String.format("Joined split %s. Added %s lbs to their order", splitId, pounds), username));
        if (openOrClosedSplit.equals(CLOSED)) {
            changeStatusToAllUserSplits(split.getId(), CLOSED);
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Split join successful"));
    }

    private ResponseEntity<Success> addToSplit(Split split, String pounds, String splitId, String username) {
        String available = calculateAvailable(split.getAvailable(), pounds);
        String openOrClosedSplit = setNewStatus(available);
        UserSplit startedUserSplit = userSplitsRepository.findBySplitIdAndUsername(splitId, username);
        startedUserSplit.setClaimed(String.valueOf(Integer.parseInt(startedUserSplit.getClaimed()) + Integer.parseInt(pounds)));
        startedUserSplit.setSplitTotal(calculateSplitTotal(startedUserSplit.getPricePerPound(), startedUserSplit.getClaimed()));
        startedUserSplit.setStatus(openOrClosedSplit);
        userSplitsRepository.save(startedUserSplit);
        split.setClaimed(calculateClaimed(split.getClaimed(), pounds));
        split.setAvailable(available);
        split.setStatus(openOrClosedSplit);
        splitsRepository.save(split);
        userLogRepository.save(buildUserLogSplit(String.format("Added %s lbs more from split %s %s", pounds, split.getId(), split.getProductDescription()), username));
        if (openOrClosedSplit.equals(CLOSED)) {
            changeStatusToAllUserSplits(split.getId(), CLOSED);
        }
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Adding more to split successful"));
    }

    private ResponseEntity<Success> removeSplitUser(String splitId, String username) {
        UserSplit userSplit = userSplitsRepository.findBySplitIdAndUsername(splitId, username);
        if (userSplit == null) {
            throw new SplitsException("No such split id");
        }
        return removeUserSplit(userSplit, username);
    }

    private Split buildSplitFromRequest(String username, Product product) {
        Split split = new Split();
        split.setStartedBy(username);
        split.setProductCode(product.getProductCode());
        split.setProductDescription(product.getProductDescription());
        split.setProductPrice(product.getProductPrice());
        split.setTotalWeight(product.getTotalWeight());
        split.setPricePerPound(calculatePricePerPound(product.getProductPrice(), product.getTotalWeight()));
        split.setClaimed("0");
        split.setAvailable(product.getTotalWeight());
        split.setStatus(OPEN);
        split.setLocked(NO);
        split.setPaid(NO);
        return split;
    }

    private UserLog buildUserLogNewSplit(String username, Product product) {
        UserLog userLog = new UserLog();
        userLog.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        userLog.setUserAction(String.format("New split %s %s", product.getProductCode(), product.getProductDescription()));
        userLog.setUsername(username);
        return userLog;
    }

    private UserLog buildUserLogSplit(String message, String username) {
        UserLog userLog = new UserLog();
        userLog.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        userLog.setUserAction(message);
        userLog.setUsername(username);
        return userLog;
    }

    private UserSplit buildSplitFromRequest(Split split, String pounds, String username, String openOrClosedSplit) {
        UserSplit userSplit = new UserSplit();
        userSplit.setSplitId(String.valueOf(split.getId()));
        userSplit.setStartedBy(split.getStartedBy());
        userSplit.setUsername(username);
        userSplit.setProductCode(split.getProductCode());
        userSplit.setProductDescription(split.getProductDescription());
        userSplit.setProductPrice(split.getProductPrice());
        userSplit.setTotalWeight(split.getTotalWeight());
        userSplit.setPricePerPound(split.getPricePerPound());
        userSplit.setClaimed(pounds);
        userSplit.setSplitTotal(calculateSplitTotal(split.getPricePerPound(), pounds));
        userSplit.setStatus(openOrClosedSplit);
        userSplit.setPaid(NO);
        return userSplit;
    }
}
