package dev.peterpetrov.sandbox.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BuyerFullOrderRow {

    private int id;
    private String tableId;
    private String type;
    private String productCode;
    private String productDescription;
    private String productPrice;
    private String status;
}
