package dev.peterpetrov.sandbox.exception;

public class FullSacksException extends RuntimeException {

    public FullSacksException(String message) {
        super(message);
    }
}
