package dev.peterpetrov.sandbox.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user_log")
public class UserLog {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "timestamp")
    private String timestamp;

    @Column(name = "username")
    private String username;

    @Column(name = "user_action")
    private String userAction;
}
