package dev.peterpetrov.sandbox.service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import dev.peterpetrov.sandbox.dto.Success;
import dev.peterpetrov.sandbox.entity.*;
import dev.peterpetrov.sandbox.exception.RandomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import dev.peterpetrov.sandbox.dto.Message;
import dev.peterpetrov.sandbox.dto.ProductCodePrice;
import dev.peterpetrov.sandbox.repository.FullSacksRepository;
import dev.peterpetrov.sandbox.repository.ProductRepository;
import dev.peterpetrov.sandbox.repository.RandomRepository;
import dev.peterpetrov.sandbox.repository.SplitsRepository;
import dev.peterpetrov.sandbox.repository.UserLogRepository;
import dev.peterpetrov.sandbox.repository.UserSplitsRepository;

@Service
public class RandomService {

    private static final String CLOSED_SPLIT = "CLOSED";
    private static final String CLOSED_BUY = "Closed";
    private static final String OPENED_BUY = "Open";

    private final RandomRepository randomRepository;
    private final FullSacksRepository fullSacksRepository;
    private final SplitsRepository splitsRepository;
    private final UserLogRepository userLogRepository;
    private final UserSplitsRepository userSplitsRepository;
    private final ProductRepository productRepository;

    @Autowired
    public RandomService(RandomRepository randomRepository, FullSacksRepository fullSacksRepository, ProductRepository productRepository,
                         SplitsRepository splitsRepository, UserLogRepository userLogRepository, UserSplitsRepository userSplitsRepository) {
        this.randomRepository = randomRepository;
        this.fullSacksRepository = fullSacksRepository;
        this.splitsRepository = splitsRepository;
        this.userLogRepository = userLogRepository;
        this.userSplitsRepository = userSplitsRepository;
        this.productRepository = productRepository;
    }

    public ResponseEntity<Success> openCloseBuy() {
        Random random = randomRepository.findById(1);
        return random.getUserAction().equals("Open") ? setBuyClosed(random) : setBuyOpen(random);
    }

    public ResponseEntity<Success> updateMessage(Message message) {
        Random random = randomRepository.findById(2);
        random.setUserAction(message.getPayload());
        randomRepository.save(random);
        return ResponseEntity.status(HttpStatus.OK).body(new Success("New message set successful"));
    }

    public ResponseEntity<Message> currentOrderCount() {
        List<FullSack> fullSacks = fullSacksRepository.findAll();
        List<Split> splits = splitsRepository.findAllByStatus(CLOSED_SPLIT);
        return ResponseEntity.status(HttpStatus.OK).body(new Message(String.valueOf(fullSacks.size() + splits.size())));
    }

    public ResponseEntity<List<UserLog>> getUserLog() {
        List<UserLog> userLogList = userLogRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(userLogList);
    }

    public ResponseEntity<Message> getBuyStatus() {
        Random random = randomRepository.findById(1);
        return ResponseEntity.status(HttpStatus.OK).body(new Message(random.getUserAction()));
    }

    public ResponseEntity<Message> getMessage() {
        Random random = randomRepository.findById(2);
        return ResponseEntity.status(HttpStatus.OK).body(new Message(random.getUserAction()));
    }

    public ResponseEntity<Message> getUserTotal(String username) {
        List<FullSack> userFullSacks = fullSacksRepository.findAllByUsername(username);
        List<UserSplit> userSplits = userSplitsRepository.findAllByUsernameAndStatus(username, CLOSED_SPLIT);
        double totalFullSacks = userFullSacks.stream().mapToDouble(fullSack -> Double.parseDouble(fullSack.getProductPrice())).sum();
        double totalSplits = userSplits.stream().mapToDouble(split -> Double.parseDouble(split.getSplitTotal())).sum();
        return ResponseEntity.status(HttpStatus.OK).body(new Message(formatPrice(totalFullSacks + totalSplits)));
    }

    public ResponseEntity<Success> setPrice(ProductCodePrice productCodePrice) {
        Product product = productRepository.findByProductCode(productCodePrice.getProductCode());
        if (product == null) {
            throw new RandomException("No such product code");
        }
        product.setProductPrice(productCodePrice.getPrice());
        productRepository.save(product);
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Product price set successfully"));
    }

    public ResponseEntity<Message> orderTotal() {
        List<FullSack> fullSacks = fullSacksRepository.findAll();
        List<Split> splits = splitsRepository.findAllByStatus(CLOSED_SPLIT);
        double totalFullSacks = fullSacks.stream().mapToDouble(fullSack -> Double.parseDouble(fullSack.getProductPrice())).sum();
        double totalSplits = splits.stream().mapToDouble(split -> Double.parseDouble(split.getProductPrice())).sum();
        return ResponseEntity.status(HttpStatus.OK).body(new Message(formatPrice(totalFullSacks + totalSplits)));
    }

    private ResponseEntity<Success> setBuyClosed(Random random) {
        random.setUserAction(CLOSED_BUY);
        randomRepository.save(random);
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Buy closed successful"));
    }

    private ResponseEntity<Success> setBuyOpen(Random random) {
        random.setUserAction(OPENED_BUY);
        randomRepository.save(random);
        return ResponseEntity.status((HttpStatus.OK)).body(new Success("Buy open successful"));
    }

    private String formatPrice(double price) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        return decimalFormat.format(price);
    }
}
