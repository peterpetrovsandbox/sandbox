package dev.peterpetrov.sandbox.exception;

public class RandomException extends RuntimeException {

    public RandomException(String message) {
        super(message);
    }
}
