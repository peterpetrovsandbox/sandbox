package dev.peterpetrov.sandbox.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "admin_log")
public class AdminLog {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "timestamp")
    private String timestamp;


    @Column(name = "exception")
    private String exception;
}
