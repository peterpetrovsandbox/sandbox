package dev.peterpetrov.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.peterpetrov.sandbox.entity.AdminLog;

@Repository
public interface AdminLogRepository extends JpaRepository<AdminLog, Integer> {

}
