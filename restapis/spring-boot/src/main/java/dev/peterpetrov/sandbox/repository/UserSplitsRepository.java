package dev.peterpetrov.sandbox.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.peterpetrov.sandbox.entity.UserSplit;

@Repository
public interface UserSplitsRepository extends JpaRepository<UserSplit, Integer> {

    @Transactional
    UserSplit findBySplitIdAndUsername(String splitId, String username);

    @Transactional
    List<UserSplit> findAllBySplitId(String splitId);

    @Transactional
    UserSplit findById(int id);

    @Transactional
    List<UserSplit> findByStatus(String status);

    @Transactional
    List<UserSplit> findAllByUsernameAndStatus(String username, String status);

    @Transactional
    void deleteAllBySplitId(String splitId);

    @Transactional
    List<UserSplit> findAllByUsername(String username);
}
