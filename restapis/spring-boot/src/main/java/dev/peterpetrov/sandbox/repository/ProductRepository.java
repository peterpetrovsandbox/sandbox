package dev.peterpetrov.sandbox.repository;

import dev.peterpetrov.sandbox.entity.Product;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Transactional
    Product findByProductCode(String productCode);
}