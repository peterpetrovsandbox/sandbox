package dev.peterpetrov.sandbox.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.peterpetrov.sandbox.entity.FullSack;

@Repository
public interface FullSacksRepository extends JpaRepository<FullSack, Integer> {

    @Transactional
    void deleteByIdAndUsername(int id, String username);

    @Transactional
    FullSack findFullSacksById(int id);

    @Transactional
    List<FullSack> findAllByUsername(String username);

    @Transactional
    List<FullSack> findAllByProductCode(String productCode);
}
