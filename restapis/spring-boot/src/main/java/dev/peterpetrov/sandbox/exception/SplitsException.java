package dev.peterpetrov.sandbox.exception;

public class SplitsException extends RuntimeException {

    public SplitsException(String message) {
        super(message);
    }
}
