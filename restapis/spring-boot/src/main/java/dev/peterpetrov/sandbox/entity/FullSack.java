package dev.peterpetrov.sandbox.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "full_sacks")
public class FullSack {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "product_description")
    private String productDescription;

    @Column(name = "product_price")
    private String productPrice;

    @Column(name = "total_weight")
    private String totalWeight;

    @Column(name = "username")
    private String username;

    @Column(name = "paid")
    private String paid;
}