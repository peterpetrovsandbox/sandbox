package dev.peterpetrov.sandbox.rest;

import java.util.List;

import dev.peterpetrov.sandbox.dto.Success;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import dev.peterpetrov.sandbox.dto.Message;
import dev.peterpetrov.sandbox.dto.ProductCodePrice;
import dev.peterpetrov.sandbox.entity.UserLog;
import dev.peterpetrov.sandbox.service.RandomService;

@RestController
@CrossOrigin
public class RandomRestController {

    private final RandomService randomService;

    @Autowired
    public RandomRestController(RandomService randomService) {
        this.randomService = randomService;
    }

    @PutMapping(path = "/open-close-buy", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> openCloseBuy() {
        return randomService.openCloseBuy();
    }

    @PutMapping(path = "/message", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> updateMessage(@RequestBody Message message) {
        return randomService.updateMessage(message);
    }

    @GetMapping(path = "/current-order-count", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> currentOrderCount() {
        return randomService.currentOrderCount();
    }

    @GetMapping(path = "/user-log", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserLog>> getUserLog() {
        return randomService.getUserLog();
    }

    @GetMapping(path = "/buy-status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> getBuyStatus() {
        return randomService.getBuyStatus();
    }

    @GetMapping(path = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> getMessage() {
        return randomService.getMessage();
    }

    @GetMapping(path = "/user-total/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> getUserTotal(@PathVariable String username) {
        return randomService.getUserTotal(username);
    }

    @GetMapping(path = "/order-total", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Message> orderTotal() {
        return randomService.orderTotal();
    }

    @PutMapping(path = "/set-price", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> setPrice(@RequestBody ProductCodePrice productCodePrice) {
        return randomService.setPrice(productCodePrice);
    }
}
