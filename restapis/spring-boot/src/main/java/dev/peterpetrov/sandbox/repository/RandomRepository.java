package dev.peterpetrov.sandbox.repository;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.peterpetrov.sandbox.entity.Random;

@Repository
public interface RandomRepository extends JpaRepository<Random, Integer> {

    @Transactional
    Random findById(int id);

}
