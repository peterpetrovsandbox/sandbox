package dev.peterpetrov.sandbox.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsersFullOrderRow {

    private int id;
    private String sackId;
    private String type;
    private String username;
    private String productCode;
    private String productDescription;
    private String productPrice;
    private String status;
    private String pricePerPound;
    private String claimed;
    private String total;
}

