package dev.peterpetrov.sandbox.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.EntityManager;

import dev.peterpetrov.sandbox.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.peterpetrov.sandbox.entity.AdminLog;
import dev.peterpetrov.sandbox.entity.FullSack;
import dev.peterpetrov.sandbox.entity.Split;
import dev.peterpetrov.sandbox.entity.UserSplit;
import dev.peterpetrov.sandbox.repository.AdminLogRepository;
import dev.peterpetrov.sandbox.repository.FullSacksRepository;
import dev.peterpetrov.sandbox.repository.SplitsRepository;
import dev.peterpetrov.sandbox.repository.UserSplitsRepository;

@Service
public class AdminTablesService {

    private final FullSacksRepository fullSacksRepository;
    private final UserSplitsRepository userSplitsRepository;
    private final SplitsRepository splitsRepository;
    private final AdminLogRepository adminLogRepository;
    private final EntityManager entityManager;

    private static final String CLOSED = "CLOSED";
    private static final String FULL_SACK = "Full Sack";
    private static final String SPLIT = "Split";

    @Autowired
    public AdminTablesService(FullSacksRepository fullSacksRepository, UserSplitsRepository userSplitsRepository, SplitsRepository splitsRepository,
                              AdminLogRepository adminLogRepository, EntityManager entityManager) {
        this.fullSacksRepository = fullSacksRepository;
        this.userSplitsRepository = userSplitsRepository;
        this.splitsRepository = splitsRepository;
        this.adminLogRepository = adminLogRepository;
        this.entityManager = entityManager;
    }

    public ResponseEntity<List<UsersFullOrderRow>> usersFullOrder() {
        List<FullSack> fullSacksList = fullSacksRepository.findAll();
        List<UserSplit> userSplitsList = userSplitsRepository.findByStatus(CLOSED);
        List<UsersFullOrderRow> usersFullOrderRowList = new ArrayList<>();
        AtomicInteger count = new AtomicInteger(0);
        fullSacksList.forEach(fullSack ->
                usersFullOrderRowList.add(new UsersFullOrderRow(count.incrementAndGet(), String.valueOf(fullSack.getId()), FULL_SACK, fullSack.getUsername(),
                        fullSack.getProductCode(), fullSack.getProductDescription(), fullSack.getProductPrice(), CLOSED, FULL_SACK, FULL_SACK, fullSack.getProductPrice())));
        userSplitsList.forEach(userSplit -> usersFullOrderRowList.add(new UsersFullOrderRow(count.incrementAndGet(), String.valueOf(userSplit.getId()), SPLIT, userSplit.getUsername(),
                userSplit.getProductCode(), userSplit.getProductDescription(), userSplit.getProductPrice(), userSplit.getStatus(), userSplit.getPricePerPound(), userSplit.getClaimed(), userSplit.getSplitTotal())));
        return ResponseEntity.status(HttpStatus.OK).body(usersFullOrderRowList);
    }

    public ResponseEntity<List<UsernameTotal>> usersTotals() {
        List<String> distinctUsernames = new ArrayList<>(getDistinctUsernames());
        List<UsernameTotal> usernameTotalList = new ArrayList<>();
        distinctUsernames.forEach(distinctUsername -> {
            List<FullSack> userFullSacks = fullSacksRepository.findAllByUsername(distinctUsername);
            List<UserSplit> userSplits = userSplitsRepository.findAllByUsernameAndStatus(distinctUsername, CLOSED);
            double userTotalFullSacks = userFullSacks.stream().mapToDouble(fullSack -> Double.parseDouble(fullSack.getProductPrice())).sum();
            double userTotalSplits = userSplits.stream().mapToDouble(split -> Double.parseDouble(split.getSplitTotal())).sum();
            usernameTotalList.add(new UsernameTotal(distinctUsername, String.valueOf(userTotalFullSacks + userTotalSplits)));
        });
        return ResponseEntity.status(HttpStatus.OK).body(usernameTotalList);
    }

    public ResponseEntity<List<BuyerFullOrderRow>> buyerFullOrder() {
        List<FullSack> fullSacksList = fullSacksRepository.findAll();
        List<Split> splitsList = splitsRepository.findAllByStatus(CLOSED);
        List<BuyerFullOrderRow> buyerFullOrderRowList = new ArrayList<>();
        AtomicInteger count = new AtomicInteger(0);
        fullSacksList.forEach(fullSack -> buyerFullOrderRowList.add(new BuyerFullOrderRow(count.incrementAndGet(), String.valueOf(fullSack.getId()), FULL_SACK, fullSack.getProductCode(),
                fullSack.getProductDescription(), fullSack.getProductPrice(), CLOSED)));
        splitsList.forEach(split -> buyerFullOrderRowList.add(new BuyerFullOrderRow(count.incrementAndGet(), String.valueOf(split.getId()), SPLIT, split.getProductCode(),
                split.getProductDescription(), split.getProductPrice(), split.getStatus())));
        return ResponseEntity.status(HttpStatus.OK).body(buyerFullOrderRowList);
    }

    public ResponseEntity<List<BuyerOrderByProductCodeRow>> buyerOrderByProductCode() {
        List<FullSack> fullSacksList = fullSacksRepository.findAll();
        List<Split> splitsList = splitsRepository.findAllByStatus(CLOSED);
        List<String> distinctProductCodes = new ArrayList<>(getDistinctProductCodes(fullSacksList, splitsList));
        List<BuyerOrderByProductCodeRow> buyerOrderByProductCodeRows = new ArrayList<>();
        distinctProductCodes.forEach(productCode -> {
            BuyerOrderByProductCodeRow buyerOrderByProductCodeRow = new BuyerOrderByProductCodeRow();
            List<FullSack> fullSacksByProductCode = fullSacksRepository.findAllByProductCode(productCode);
            List<Split> splitSacksByProductCode = splitsRepository.findAllByProductCodeAndStatus(productCode, CLOSED);
            addFullSack(splitSacksByProductCode, fullSacksByProductCode, buyerOrderByProductCodeRow, productCode);
            buyerOrderByProductCodeRows.add(buyerOrderByProductCodeRow);
        });
        return ResponseEntity.status(HttpStatus.OK).body(buyerOrderByProductCodeRows);
    }

    public ResponseEntity<List<AdminLog>> adminLog() {
        List<AdminLog> adminLogList = adminLogRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(adminLogList);
    }


    @Transactional
    public ResponseEntity<Success> clearOrder() {
        entityManager.createNativeQuery("TRUNCATE TABLE full_sacks").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE user_splits").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE splits").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE admin_log").executeUpdate();
        entityManager.createNativeQuery("TRUNCATE TABLE user_log").executeUpdate();
        return ResponseEntity.status(HttpStatus.OK).body(new Success("Order cleared successful"));
    }

    private void addFullSack(List<Split> splitSacksByProductCode, List<FullSack> fullSacksByProductCode, BuyerOrderByProductCodeRow buyerOrderByProductCodeRow, String productCode) {
        if (!fullSacksByProductCode.isEmpty()) {
            setRow(buyerOrderByProductCodeRow, productCode, fullSacksByProductCode);
        }
        addSplit(splitSacksByProductCode, fullSacksByProductCode, buyerOrderByProductCodeRow, productCode);
    }

    private void addSplit(List<Split> splitSacksByProductCode, List<FullSack> fullSacksByProductCode, BuyerOrderByProductCodeRow buyerOrderByProductCodeRow, String productCode) {
        if (!splitSacksByProductCode.isEmpty()) {
            if (!fullSacksByProductCode.isEmpty()) {
                buyerOrderByProductCodeRow.setQuantity(buyerOrderByProductCodeRow.getQuantity() + splitSacksByProductCode.size());
                buyerOrderByProductCodeRow.setTotal(buyerOrderByProductCodeRow.getProductPrice() * buyerOrderByProductCodeRow.getQuantity());
            } else {
                setRowSplit(buyerOrderByProductCodeRow, productCode, splitSacksByProductCode);
            }
        }
    }

    private void setRow(BuyerOrderByProductCodeRow buyerOrderByProductCodeRow, String productCode, List<FullSack> fullSacksByProductCode) {
        buyerOrderByProductCodeRow.setProductCode(productCode);
        buyerOrderByProductCodeRow.setProductDescription(fullSacksByProductCode.get(0).getProductDescription());
        buyerOrderByProductCodeRow.setProductPrice(Double.valueOf(fullSacksByProductCode.get(0).getProductPrice()));
        buyerOrderByProductCodeRow.setQuantity(fullSacksByProductCode.size());
        buyerOrderByProductCodeRow.setTotal(Double.parseDouble(fullSacksByProductCode.get(0).getProductPrice()) * fullSacksByProductCode.size());
    }

    private void setRowSplit(BuyerOrderByProductCodeRow buyerOrderByProductCodeRow, String productCode, List<Split> splitSacksByProductCode) {
        buyerOrderByProductCodeRow.setProductCode(productCode);
        buyerOrderByProductCodeRow.setTotal(Double.parseDouble(splitSacksByProductCode.get(0).getProductPrice()) * splitSacksByProductCode.size());
        buyerOrderByProductCodeRow.setProductDescription(splitSacksByProductCode.get(0).getProductDescription());
        buyerOrderByProductCodeRow.setProductPrice(Double.valueOf(splitSacksByProductCode.get(0).getProductPrice()));
        buyerOrderByProductCodeRow.setQuantity(splitSacksByProductCode.size());
    }

    private Set<String> getDistinctProductCodes(List<FullSack> fullSacks, List<Split> splits) {
        Set<String> productCodeSet = new HashSet<>();
        fullSacks.forEach(fullSack -> productCodeSet.add(fullSack.getProductCode()));
        splits.forEach(split -> productCodeSet.add(split.getProductCode()));
        return productCodeSet;
    }

    private Set<String> getDistinctUsernames() {
        List<FullSack> fullSacks = fullSacksRepository.findAll();
        List<UserSplit> userSplits = userSplitsRepository.findByStatus(CLOSED);
        Set<String> usernames = new HashSet<>();
        fullSacks.forEach(fullSack -> usernames.add(fullSack.getUsername()));
        userSplits.forEach(userSplit -> usernames.add(userSplit.getUsername()));
        return usernames;
    }
}
