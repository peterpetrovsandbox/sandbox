package dev.peterpetrov.sandbox.rest;

import java.util.List;

import dev.peterpetrov.sandbox.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.peterpetrov.sandbox.entity.AdminLog;
import dev.peterpetrov.sandbox.service.AdminTablesService;

@RestController
@CrossOrigin
public class AdminTablesRestController {

    private final AdminTablesService adminTablesService;

    @Autowired
    public AdminTablesRestController(AdminTablesService adminTablesService) {
        this.adminTablesService = adminTablesService;
    }

    @GetMapping(path = "/users-full-order", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsersFullOrderRow>> usersFullOrder() {
        return adminTablesService.usersFullOrder();
    }

    @GetMapping(path = "/users-totals", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UsernameTotal>> usersTotals() {
        return adminTablesService.usersTotals();
    }

    @GetMapping(path = "/buyer-full-order", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BuyerFullOrderRow>> buyerFullOrder() {
        return adminTablesService.buyerFullOrder();
    }

    @GetMapping(path = "/buyer-order-by-product-code", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BuyerOrderByProductCodeRow>> buyerOrderByProductCode() {
        return adminTablesService.buyerOrderByProductCode();
    }

    @PostMapping(path = "/reset-order", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Success> resetOrder() {
        return adminTablesService.clearOrder();
    }

    @GetMapping(path = "/admin-log", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AdminLog>> adminLog() {
        return adminTablesService.adminLog();
    }
}
