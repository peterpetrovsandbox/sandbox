package dev.peterpetrov.sandbox.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.peterpetrov.sandbox.entity.Split;

@Repository
public interface SplitsRepository extends JpaRepository<Split, Integer> {

    @Transactional
    Split findSplitsByProductCodeAndStatus(String productCode, String status);

    @Transactional
    Split findById(int id);

    @Transactional
    List<Split> findByStatus(String status);

    @Transactional
    List<Split> findAllByStatus(String status);

    @Transactional
    List<Split> findAllByProductCodeAndStatus(String productCode, String status);

    @Transactional
    long countAllByStatus(String status);
}
