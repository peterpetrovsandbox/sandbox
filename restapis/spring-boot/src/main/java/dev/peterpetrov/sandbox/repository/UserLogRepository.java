package dev.peterpetrov.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.peterpetrov.sandbox.entity.UserLog;

@Repository
public interface UserLogRepository extends JpaRepository<UserLog, Integer> {

}
