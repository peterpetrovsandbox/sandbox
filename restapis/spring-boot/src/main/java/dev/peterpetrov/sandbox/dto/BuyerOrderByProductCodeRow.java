package dev.peterpetrov.sandbox.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BuyerOrderByProductCodeRow {

    private String productCode;
    private String productDescription;
    private Double productPrice;
    private int quantity;
    private Double total;
}
