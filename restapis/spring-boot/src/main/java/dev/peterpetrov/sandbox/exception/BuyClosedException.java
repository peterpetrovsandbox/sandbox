package dev.peterpetrov.sandbox.exception;

public class BuyClosedException extends RuntimeException {

    public BuyClosedException(String message) {
        super(message);
    }
}
