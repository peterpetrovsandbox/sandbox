package dev.peterpetrov.sandbox.rest.exceptionhandler;

import dev.peterpetrov.sandbox.dto.ExceptionMessage;
import dev.peterpetrov.sandbox.entity.AdminLog;
import dev.peterpetrov.sandbox.exception.RandomException;
import dev.peterpetrov.sandbox.exception.SplitsException;
import dev.peterpetrov.sandbox.exception.BuyClosedException;
import dev.peterpetrov.sandbox.exception.FullSacksException;
import dev.peterpetrov.sandbox.repository.AdminLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.sql.Timestamp;

@ControllerAdvice
public class RestExceptionHandler {

    private final AdminLogRepository adminLogRepository;

    @Autowired
    public RestExceptionHandler(AdminLogRepository adminLogRepository) {
        this.adminLogRepository = adminLogRepository;
    }

    public static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<ExceptionMessage> handleBuyClosedException(BuyClosedException e) {
        return exceptionResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionMessage> handleFullSacksException(FullSacksException e) {
        return exceptionResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionMessage> handleSplitsException(SplitsException e) {
        return exceptionResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionMessage> handleRandomException(RandomException e) {
        return exceptionResponse(HttpStatus.BAD_REQUEST, e);
    }

    @ExceptionHandler
    public ResponseEntity<ExceptionMessage> handleUnexpectedException(Exception e) {
        String message = "Unexpected error when executing request";
        adminLogRepository.save(buildAdminLog(e));
        return exceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, new Exception(message, e));
    }

    private ResponseEntity<ExceptionMessage> exceptionResponse(HttpStatus status, Exception e) {
        logger.error(e.getMessage(), e);
        ExceptionMessage response = new ExceptionMessage(e.getMessage());
        return ResponseEntity.status(status).body(response);
    }

    private AdminLog buildAdminLog(Exception e) {
        AdminLog adminLog = new AdminLog();
        adminLog.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        adminLog.setException(e.toString());
        return adminLog;
    }
}
